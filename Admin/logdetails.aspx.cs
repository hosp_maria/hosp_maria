﻿//User log details by date and by specific user can be viewed here
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Admin_logdetails : System.Web.UI.Page
{
    conclass c = new conclass();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Admin/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            MultiView1.ActiveViewIndex = 0;
            this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
            lbl_no.Visible = false;
        }
    }

    protected void btn_srch_Click(object sender, EventArgs e)
    {
        c.getCon();
        MultiView1.ActiveViewIndex = 3;

        SqlCommand cmd = new SqlCommand("select * from LogTable l inner join UserTable u on u.username=l.username where u.username='" + txt_user.Text + "' and l.datentime between '" + Convert.ToDateTime(txt_date.Text) + "' and '" + DateTime.Now + "' order by l.datentime", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {

            gridview_emp.DataSource = dt;
            gridview_emp.DataBind();
        }
        else
            lbl_no.Visible = true;
    }
    protected void btn_srch2_Click(object sender, EventArgs e)
    {
        c.getCon();
        MultiView1.ActiveViewIndex = 3;

        SqlCommand cmd = new SqlCommand("select * from LogTable l inner join UserTable u on u.username=l.username where  l.datentime between '" + Convert.ToDateTime(txt_from.Text) + "' and '" + Convert.ToDateTime(txt_to.Text) + "' order by l.datentime", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {

            gridview_emp.DataSource = dt;
            gridview_emp.DataBind();
        }
        else
            lbl_no.Visible = true;
    }

   
    protected void link_user_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 1;

    }
    protected void link_time_Click1(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 2;
    }
}