﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminHomeMaster.master" AutoEventWireup="true" CodeFile="changeusername.aspx.cs" Inherits="Admin_changeusername" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 23px;
        }
        .auto-style3 {
            font-size: medium;
        }
        .auto-style4 {
            height: 23px;
            font-size: medium;
        }
    </style>
<link href="style.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <table class="auto-style1">
            <tr>
                <td>
                    <asp:Label ID="lbl_title" runat="server" CssClass="auto-style3"></asp:Label>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style4">Enter New UserName</td>
                <td class="auto-style2">
                    <asp:TextBox ID="txt_usr" runat="server"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_usr" ErrorMessage="*Invalid Username:length must be between 7 to 10 characters" ForeColor="#CC0000" ValidationExpression="^[a-zA-Z0-9'@&amp;#.\s]{7,10}$"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td class="auto-style3">Enter Password</td>
                <td>
                    <asp:TextBox ID="txt_pass" runat="server" TextMode="Password"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_pass" ErrorMessage="*Enter Password" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btn_usr" runat="server" OnClick="btn_usr_Click" Text="Change Username" ForeColor="Gray" />
                </td>
                <td>
                    <asp:Button ID="btn_cancel" runat="server" OnClick="btn_cancel_Click" Text="Cancel" Width="118px" />
                </td>
            </tr>
            
            
            
        </table>
    <div>
    
    </div>
    </form>
</asp:Content>
