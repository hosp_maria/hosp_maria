﻿//New Unapproved Employees are viewed here
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Admin_ViewNewEmps : System.Web.UI.Page
{
    conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Admin/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            BindGridViewcas();
            lbl_nocas.Visible = false;
            lbl_noncd.Visible = false;
            lbl_noop.Visible = false;
            BindGridViewop();
            BindGridViewncd();
            MultiView1.ActiveViewIndex = 0;
            Bindlbl();
        }
    }
    private void Bindlbl()
    {
         c.getCon();
        string status = "Pending";
        SqlCommand cmd = new SqlCommand("select count(*) from CasualityEmployeeDetails e where status='" + status + "'", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            if (Convert.ToInt32(dt.Rows[0][0]) == 0)
                lbl_cas.Text = "No New Employees in Casuality";
            else
            {
                lbl_cas.ForeColor = System.Drawing.Color.Red;
                lbl_cas.Text = dt.Rows[0][0].ToString() + " New Employees in Casuality";
            }
            
        }
      
        SqlCommand cmds = new SqlCommand("select count(*) from EmployeeDetails e where status='" + status + "'", c.Con);
        DataTable dts = new DataTable();
        SqlDataAdapter das = new SqlDataAdapter(cmds);
        das.Fill(dts);


        if (dts.Rows.Count > 0)
        {
            if (Convert.ToInt32(dts.Rows[0][0]) == 0)
                lbl_op.Text = "No New Employees in OP";
            else
            {
                lbl_op.ForeColor = System.Drawing.Color.Red;
                lbl_op.Text = dts.Rows[0][0].ToString() + " New Employees in OP";
            }
            
        }
       
        SqlCommand cmdt = new SqlCommand("select count(*) from NCDEmployeeDetails e where status='" + status + "'", c.Con);
        DataTable dtt = new DataTable();
        SqlDataAdapter dat = new SqlDataAdapter(cmdt);
        dat.Fill(dtt);


        if (dtt.Rows.Count > 0)
        {
            if (Convert.ToInt32(dtt.Rows[0][0]) == 0)
                lbl_ncd.Text = "No New Employees in NCD";
            else
            {
                lbl_ncd.ForeColor = System.Drawing.Color.Red;
                lbl_ncd.Text = dtt.Rows[0][0].ToString() + " New Employees in NCD";
            }
        }
        c.Con.Close();


       
    }
    private void BindGridViewcas()
    {
        c.getCon();
        string status = "Pending";
        SqlCommand cmd = new SqlCommand("select * from CasualityEmployeeDetails e inner join Designation d on d.desigid=e.desigid where status='" + status + "'", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            gridview_empcas.DataSource = dt;
            gridview_empcas.DataBind();
        }
        else
        {
            lbl_nocas.Visible = true;
        }
        c.Con.Close();

    }
    protected void gridview_empcas_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ApproveButton")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = gridview_empcas.Rows[index];
            Response.Redirect("approveusercas.aspx?EmpNo=" + row.Cells[0].Text);
        }
        if (e.CommandName == "DeleteButton")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = gridview_empcas.Rows[index];
            Response.Redirect("deleteusercas.aspx?EmpNo=" + row.Cells[0].Text);
        }
    }
    private void BindGridViewop()
    {
        c.getCon();
        string status = "Pending";
        SqlCommand cmd = new SqlCommand("select * from EmployeeDetails e inner join Designation d on d.desigid=e.desigid where status='" + status + "'", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            gridview_empop.DataSource = dt;
            gridview_empop.DataBind();
        }
        else
        {
            lbl_noop.Visible = true;
        }
        c.Con.Close();

    }
    protected void gridview_empop_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ApproveButton")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = gridview_empop.Rows[index];
            Response.Redirect("approveuseropd.aspx?EmpNo=" + row.Cells[0].Text);
        }
        if (e.CommandName == "DeleteButton")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = gridview_empop.Rows[index];
            Response.Redirect("deleteuseropd.aspx?EmpNo=" + row.Cells[0].Text);
        }
    }
    private void BindGridViewncd()
    {
        c.getCon();
        string status = "Pending";
        SqlCommand cmd = new SqlCommand("select * from NCDEmployeeDetails e inner join Designation d on d.desigid=e.desigid where status='" + status + "'", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            gridview_emp.DataSource = dt;
            gridview_emp.DataBind();
        }
        else
        {
            lbl_noncd.Visible = true;
        }
        c.Con.Close();
    }
    protected void gridview_emp_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ApproveButton")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = gridview_emp.Rows[index];
            Response.Redirect("approveuserncd.aspx?EmpNo=" + row.Cells[0].Text);
        }
        if (e.CommandName == "DeleteButton")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = gridview_emp.Rows[index];
            Response.Redirect("deleteuserncd.aspx?EmpNo=" + row.Cells[0].Text);
        }
    }



    protected void link_op_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 0;
    }
    protected void link_ncd_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 1;
    }
    protected void link_cas_Click(object sender, EventArgs e)
    {
        MultiView1.ActiveViewIndex = 2;

    }
}