﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminHomeMaster.master" AutoEventWireup="true" CodeFile="homepageadmin.aspx.cs" Inherits="Admin_homepageadmin" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
  <form id="form1" >
    <div>
        <section class= "featured-service-content">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 service">
						<div class="row text-center">
							<div class="service-icon text-center">
		        				<i class="fa fa-5x fa-eye"></i>
		        			</div>
						</div>
		        			
	        			<div class="row text-center">
	        				<div class="about-service">
		        				<h3 class="text-center"><a href="ViewNewEmps.aspx" class="rm-btn btn btn-primary">
										View New Employees</a></h3>
		        				<%--<p class="text-center">
		        					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		                       		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		                       		quis nostrud 
		        				</p>--%>
		        			</div>
	        			</div>
		 						
	        		</div>	<!-- col-sm-4 -->

	        		<div class="col-sm-4 service">
	        			<div class="row text-center">
	        				<div class="service-icon text-center">
		        				<i class="fa fa-male fa-5x"></i>
		        			</div>
	        			</div>
		        			
	        			<div class="row text-center">
	        				<div class="about-service">
		        				<h3 class="text-center"> <a href="AddUserAfterSearch.aspx" class="rm-btn btn btn-primary">
										Add User</a></h3>
		        				<%--<p class="text-center">
		        					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		                       		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		                       		quis nostrud 
		        				</p>--%>
		        			</div>
	        			</div>
		        			
	        		</div>	<!-- col-sm-4 -->
	        		
	        		<div class="col-sm-4 service">
	        			<div class="row text-center">
	        				<div class="service-icon text-center">
		        				<i class="fa fa-lock fa-5x"></i>
		        			</div>
	        			</div>
		        			
	        			<div class="row text-center">
	        				<div class="about-service">
		        				<h3 class="text-center"><a href="logout.aspx" class="rm-btn btn btn-primary">
										Logout</a></h3>
		        				<%--<p class="text-center">
		        					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		                       		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		                       		quis nostrud 
		        				</p>--%>
                                
		        			</div>
	        			</div>
		        			
	        		</div>	<!-- col-sm-4 -->
	        	
	        		</div>
			</div>
		</section>
      <%--  <table class="auto-style1">
            <tr>
                <td class="auto-style2">&nbsp;</td>
                <td class="auto-style2"></td>
                <td class="auto-style2"></td>
            </tr>
            <tr>
                <td>
                    <asp:ImageButton ID="ImageButton2" runat="server" width="100" height="100" ImageUrl="~/Casuality/Doctor/images/pat.png" OnClick="ImageButton2_Click" />
                    <br />
                    </td>
                <td>
                    <asp:ImageButton ID="ImageButton1" runat="server" width="100" height="100" ImageUrl="~/OPD/Doctor/images/ref.png" OnClick="ImageButton1_Click" />
                    <br />
                    View Refferals</td>
                <td>
                    <asp:ImageButton ID="ImageButton3" runat="server" width="100" height="100" ImageUrl="~/Casuality/Doctor/images/logouts.png" OnClick="ImageButton3_Click"/>
                    <br />
                    Logout</td>
            </tr>
        </table>--%>

        <br />

    
    
          </div>
    </form>
</asp:Content>

