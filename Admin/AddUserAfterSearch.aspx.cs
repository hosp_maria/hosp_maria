﻿//Add Users after searching the employee
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Net.Mail;
public partial class Admin_AddUserAfterSearch : System.Web.UI.Page
{
    conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Admin/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
            // pnlPopup.Visible = false;
            lbl_type.Visible = false;
        }
    }
    private void CreateRandomPassword()
    {
        string allowedChars = "";

        allowedChars = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";

        allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";

        allowedChars += "1,2,3,4,5,6,7,8,9,0,!,@,#,$,%,&,?";

        char[] sep = { ',' };

        string[] arr = allowedChars.Split(sep);

        string passwordString = "";

        string temp = "";

        Random rand = new Random();

        for (int i = 0; i < 10; i++)
        {

            temp = arr[rand.Next(0, arr.Length)];

            passwordString += temp;

        }

        lbl_pass.Text = Convert.ToString(passwordString);
        lbl_user.Text = Convert.ToString(passwordString);
    }
    //protected void btn_submit_Click(object sender, EventArgs e)
    //{
    //    pnlPopup.Visible = true;
    //    ModalPopupExtender1.Show();   

    // }
    public void sentmail()
    {
        string p = txt_email.Text.Trim();
        MailMessage msg = new MailMessage();
        msg.From = new MailAddress("govhospchanganacherry@gmail.com");
        msg.To.Add(new MailAddress(p));
        msg.Subject = "Hospital Management System Account Approved";
        msg.Body = string.Format("<html><head></head><body><b>Hi,</b></br><p>Your Account has been approved..Your username is <b> '" + lbl_user.Text + "'</b> and your password is <b>'" + lbl_pass.Text + "'</p></b> <p> You can change your username and password after logging into the system with the specified credentials</p></body></html>");
        msg.IsBodyHtml = true;
        SmtpClient smtp = new SmtpClient();
        smtp.EnableSsl = true;
        smtp.Send(msg);

        // ScriptManager.RegisterStartupScript(Page, this.GetType(), "write", "alert('Registered successfully')", true);
    }
    
    protected void btn_search_Click(object sender, EventArgs e)
    {
        c.getCon();
        if (ddl_mod.SelectedIndex != 0)
        {
            if (ddl_mod.SelectedItem.Text == "NCD")
            {
                SqlCommand cmd111 = new SqlCommand("select * from NCDEmployeeDetails where empid= '" + txt_id.Text + "' ", c.Con);
                SqlDataAdapter sda111 = new SqlDataAdapter(cmd111);
                DataTable dt111 = new DataTable();
                sda111.Fill(dt111);

                int k = cmd111.ExecuteNonQuery();

                if (dt111.Rows.Count > 0)
                {
                    DataRow row11 = dt111.Rows[dt111.Rows.Count - 1];

                    String name = Convert.ToString(row11[2]);
                    String eid = Convert.ToString(row11[0]);
                    String etype = Convert.ToString(row11[1]);
                    String email = Convert.ToString(row11[18]);
                    txt_name.Text = name;
                    txt_id.Text = eid;
                    txt_typeid.Text = etype;
                    txt_email.Text = email;
                }
                else
                    Response.Write("<script>alert('Employee Not Found');</script>");


            }
            else if (ddl_mod.SelectedItem.Text == "Casuality")
            {
                SqlCommand cmd111 = new SqlCommand("select * from CasualityEmployeeDetails where empid= '" + txt_id.Text + "' ", c.Con);
                SqlDataAdapter sda111 = new SqlDataAdapter(cmd111);
                DataTable dt111 = new DataTable();
                sda111.Fill(dt111);

                int k = cmd111.ExecuteNonQuery();

                if (dt111.Rows.Count > 0)
                {
                    DataRow row11 = dt111.Rows[dt111.Rows.Count - 1];

                    String name = Convert.ToString(row11[2]);
                    String eid = Convert.ToString(row11[0]);
                    String etype = Convert.ToString(row11[1]);
                    String email = Convert.ToString(row11[18]);
                    txt_name.Text = name;
                    txt_id.Text = eid;
                    txt_typeid.Text = etype;
                    txt_email.Text = email;
                }
                else
                    Response.Write("<script>alert('Employee Not Found');</script>");

            }
            else
            {
                SqlCommand cmd111 = new SqlCommand("select * from EmployeeDetails where empid= '" + txt_id.Text + "' ", c.Con);
                SqlDataAdapter sda111 = new SqlDataAdapter(cmd111);
                DataTable dt111 = new DataTable();
                sda111.Fill(dt111);

                int k = cmd111.ExecuteNonQuery();

                if (dt111.Rows.Count > 0)
                {
                    DataRow row11 = dt111.Rows[dt111.Rows.Count - 1];

                    String name = Convert.ToString(row11[2]);
                    String eid = Convert.ToString(row11[0]);
                    String etype = Convert.ToString(row11[1]);
                    String email = Convert.ToString(row11[18]);
                    txt_name.Text = name;
                    txt_id.Text = eid;
                    txt_typeid.Text = etype;
                    txt_email.Text = email;
                }
                else
                    Response.Write("<script>alert('Employee Not Found');</script>");

            }
            c.Con.Close();
            CreateRandomPassword();
        }
        else
            lbl_type.Visible = true;

    }
    protected void btb_OK_Click(object sender, EventArgs e)
    {
        Response.Redirect("AddUserAfterSearch.aspx");
    }
    protected void btn_add_Click(object sender, EventArgs e)
    {
        c.getCon();
        String s = "insert into UserTable values('" + lbl_user.Text + "','" + lbl_pass.Text + "','" + txt_id.Text + "','" + txt_typeid.Text + "','" + ddl_mod.SelectedItem.Text + "')";
        SqlCommand cmd1 = new SqlCommand(s, c.Con);
        cmd1.ExecuteNonQuery();
        String status = "Approved";
        SqlCommand cmd = new SqlCommand("update CasualityEmployeeDetails set status='" + status + "' where empid='" + txt_id.Text + "'", c.Con);

        int result = cmd.ExecuteNonQuery();

      //  sentmail();
        Panel_Popup.Visible = true;
        btn_add_ModalPopupExtender.Show();
        c.Con.Close();
        //Response.Write("<script>alert('User Created Successfully');</script>");
        
    }
}