﻿//Add OP Employee user
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Net.Mail;
public partial class Admin_adduserpageopd : System.Web.UI.Page
{
    conclass c = new conclass();
    int empno = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Admin/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
        empno = Convert.ToInt32(Request.QueryString["EmpNo"].ToString());
        CreateRandomPassword();
      
            BindTextBoxvalues();
            lbl_type.Visible = false;

        }
    }
    public void sentmail()
    {
        string p = txt_email.Text.Trim();
        MailMessage msg = new MailMessage();
        msg.From = new MailAddress("govhospchanganacherry@gmail.com");
        msg.To.Add(new MailAddress(p));
        msg.Subject = "Hospital Management System Account Approved";
        msg.Body = string.Format("<html><head></head><body><b>Hi,</b></br><p>Your Account has been approved..Your username is <b> '" + lbl_user.Text + "'</b> and your password is <b>'" + lbl_pass.Text + "'</p></b> <p> You can change your username and password after logging into the system with the specified credentials</p></body></html>");
        msg.IsBodyHtml = true;
        SmtpClient smtp = new SmtpClient();
        smtp.EnableSsl = true;
        smtp.Send(msg);

        // ScriptManager.RegisterStartupScript(Page, this.GetType(), "write", "alert('Registered successfully')", true);
    }
    private void BindTextBoxvalues()
    {

        c.getCon();
        SqlCommand cmd = new SqlCommand("select * from EmployeeDetails where empid='" + empno + "'", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        txt_id.Text = dt.Rows[0][0].ToString();
        txt_name.Text = dt.Rows[0][2].ToString();
        txt_email.Text = dt.Rows[0][18].ToString();
        txt_typeid.Text = dt.Rows[0][1].ToString();


    }
    private void CreateRandomPassword()
    {
        string allowedChars = "";

        allowedChars = "a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,";

        allowedChars += "A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,";

        allowedChars += "1,2,3,4,5,6,7,8,9,0,!,@,#,$,%,&,?";

        char[] sep = { ',' };

        string[] arr = allowedChars.Split(sep);

        string passwordString = "";

        string temp = "";

        Random rand = new Random();

        for (int i = 0; i < 10; i++)
        {

            temp = arr[rand.Next(0, arr.Length)];

            passwordString += temp;

        }

        lbl_pass.Text = Convert.ToString(passwordString);
        lbl_user.Text = Convert.ToString(passwordString);

    }
    protected void btn_submit_Click(object sender, EventArgs e)
    {
        if (ddl_mod.SelectedIndex != 0)
        {
            c.getCon();
            String s = "insert into UserTable values('" + lbl_user.Text + "','" + lbl_pass.Text + "','" + txt_id.Text + "','" + txt_typeid.Text + "','" + ddl_mod.SelectedItem.Text + "')";
            SqlCommand cmd1 = new SqlCommand(s, c.Con);
            cmd1.ExecuteNonQuery();
            String status = "Approved";
            SqlCommand cmd = new SqlCommand("update EmployeeDetails set status='" + status + "' where empid='" + txt_id.Text + "'", c.Con);
            cmd.ExecuteNonQuery();
            sentmail();
            c.Con.Close();
            Response.Write("<script>alert('User Created Successfully');</script>");
        }
        else
            lbl_type.Visible = true;
    }



}
