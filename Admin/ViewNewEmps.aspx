﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminHomeMaster.master" AutoEventWireup="true" CodeFile="ViewNewEmps.aspx.cs" Inherits="Admin_ViewNewEmps" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            height: 20px;
        }
    </style>
    <link href="../assets/css/flexslider.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <table class="nav-justified">
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbl_op" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbl_ncd" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbl_cas" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style1"></td>
            </tr>
            <tr>
                <td><div class="drop">
<ul class="drop_menu">
     <li><asp:LinkButton ID="link_op" runat="server" OnClick="link_op_Click">OP Employees</asp:LinkButton>
    </li>
    <li><asp:LinkButton ID="link_ncd" runat="server" OnClick="link_ncd_Click">NCD Employees</asp:LinkButton>
    </li>
      <li><asp:LinkButton ID="link_cas" runat="server" OnClick="link_cas_Click">Casuality Employees</asp:LinkButton>
    </li>
  </ul></div></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:MultiView ID="MultiView1" runat="server">
                        <table class="nav-justified">
                            <tr>
                                <td>
                                    <asp:View ID="View1" runat="server">
                                        <table class="nav-justified">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gridview_empop" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White" OnRowCommand="gridview_empop_RowCommand">
                                                        <Columns>
                                                            <asp:BoundField DataField="empid" HeaderText="Employee Id" />
                                                            <asp:BoundField DataField="empname" HeaderText="Employee Name" />
                                                            <asp:BoundField DataField="designame" HeaderText="Designation " />
                                                            <asp:ButtonField CommandName="ApproveButton" ItemStyle-Width="30" Text="Approve">
                                                            <ItemStyle Width="30px" />
                                                            </asp:ButtonField>
                                                            <asp:ButtonField CommandName="DeleteButton" ItemStyle-Width="30" Text="Delete">
                                                            <ItemStyle Width="30px" />
                                                            </asp:ButtonField>
                                                        </Columns>
                                                        <FooterStyle BackColor="White" ForeColor="#000066" />
                                                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                        <RowStyle ForeColor="#000066" />
                                                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbl_noop" runat="server" ForeColor="Red" Text="No new employees"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:View ID="View2" runat="server">
                                        <table class="nav-justified">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gridview_emp" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White" OnRowCommand="gridview_emp_RowCommand">
                                                        <Columns>
                                                            <asp:BoundField DataField="empid" HeaderText="Employee Id" />
                                                            <asp:BoundField DataField="empname" HeaderText="Employee Name" />
                                                            <asp:BoundField DataField="designame" HeaderText="Designation " />
                                                            <asp:ButtonField CommandName="ApproveButton" ItemStyle-Width="30" Text="Approve">
                                                            <ItemStyle Width="30px" />
                                                            </asp:ButtonField>
                                                            <asp:ButtonField CommandName="DeleteButton" ItemStyle-Width="30" Text="Delete">
                                                            <ItemStyle Width="30px" />
                                                            </asp:ButtonField>
                                                        </Columns>
                                                        <FooterStyle BackColor="White" ForeColor="#000066" />
                                                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                        <RowStyle ForeColor="#000066" />
                                                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbl_noncd" runat="server" ForeColor="Red" Text="No new employees"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:View ID="View3" runat="server">
                                        <table class="nav-justified">
                                            <tr>
                                                <td>
                                                    <asp:GridView ID="gridview_empcas" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White" OnRowCommand="gridview_empcas_RowCommand">
                                                        <Columns>
                                                            <asp:BoundField DataField="empid" HeaderText="Employee Id" />
                                                            <asp:BoundField DataField="empname" HeaderText="Employee Name" />
                                                            <asp:BoundField DataField="designame" HeaderText="Designation " />
                                                            <asp:ButtonField CommandName="ApproveButton" ItemStyle-Width="30" Text="Approve">
                                                            <ItemStyle Width="30px" />
                                                            </asp:ButtonField>
                                                            <asp:ButtonField CommandName="DeleteButton" ItemStyle-Width="30" Text="Delete">
                                                            <ItemStyle Width="30px" />
                                                            </asp:ButtonField>
                                                        </Columns>
                                                        <FooterStyle BackColor="White" ForeColor="#000066" />
                                                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                        <RowStyle ForeColor="#000066" />
                                                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                    </asp:GridView>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:Label ID="lbl_nocas" runat="server" ForeColor="Red" Text="No new employees"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </td>
                            </tr>
                        </table>
                    </asp:MultiView>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>
</asp:Content>

