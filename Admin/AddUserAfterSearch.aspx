﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminHomeMaster.master" AutoEventWireup="true" CodeFile="AddUserAfterSearch.aspx.cs" Inherits="Admin_AddUserAfterSearch" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <style type="text/css">
       .modalBackground
    {
        background-color: Black;
        filter: alpha(opacity=60);
        opacity: 0.6;
    }
    .modalPopup
    {
        background-color: #FFFFFF;
        border: 3px solid #0DA9D0;
        border-radius: 12px;
        padding:0
        }
    .modalPopup .header
    {
        background-color: #2FBDF1;
        height: 30px;
        color: White;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
    }
    .modalPopup .body
    {
        min-height: 50px;
        line-height: 30px;
        text-align: center;
        font-weight: bold;
    }
    .modalPopup .footer
    {
        padding: 6px;
    }
    .modalPopup .yes
    {
        background-color: #d2f1ec;
        border: 1px solid #0DA9D0;
    }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server"><center>
        <table class="nav-justified">
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
         
<tr>
                            <td>&nbsp;</td>
                            <td><asp:Button ID ="Button1" runat="server" style =" display:none;" Text="Button"/>
                            </td>
                            <td>&nbsp;</td></tr>
            <tr>
                <td>Select Module of User</td>
                <td>
                    <asp:DropDownList ID="ddl_mod" runat="server" >
                        <asp:ListItem>[Select]</asp:ListItem>
                        <asp:ListItem>OPD</asp:ListItem>
                        <asp:ListItem>Fever Clinic</asp:ListItem>
                        <asp:ListItem>RMO</asp:ListItem>
                        <asp:ListItem>Superintendent</asp:ListItem>
                        <asp:ListItem>Pharmacy</asp:ListItem>
                        <asp:ListItem>Lab</asp:ListItem>
                        <asp:ListItem>Admin</asp:ListItem>
                        <asp:ListItem>NCD</asp:ListItem>
                        <asp:ListItem>Casuality</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:Label ID="lbl_type" runat="server" ForeColor="#CC0000" Text="*Select Module of User"></asp:Label>
                </td>
            </tr>
             
            <tr>
                <td class="style3" colspan="2">
                    &nbsp;</td>
                <td class="style3">
                    &nbsp;</td>
            </tr>
            
            <tr>
                <td class="auto-style3">
                    Employee ID</td>
                <td>
                    <asp:TextBox ID="txt_id" runat="server" Height="22px" style="margin-top: 0px" 
                        Width="164px" ></asp:TextBox>
                &nbsp;</td>
                <td>
                    <asp:RequiredFieldValidator ID="reqfield1" runat="server" ControlToValidate="txt_id" ErrorMessage="**Enter Employee ID" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                    <asp:Button ID="btn_search" runat="server" OnClick="btn_search_Click" Text="Search" CssClass="btn" />
                </td>
            </tr>
            <tr>
                <td class="auto-style3">
                    Employee Name</td>
                <td>
                    <asp:TextBox ID="txt_name" runat="server"  Width="162px" ReadOnly="True" ></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">
                    Email</td>
                <td>
                    <asp:TextBox ID="txt_email" runat="server" Height="24px" Width="164px"  ></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">
                    Emplyee Type ID</td>
                <td>
                    <asp:TextBox ID="txt_typeid" runat="server" Height="26px" Width="162px" ReadOnly="True"   ></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style5">
                    &nbsp;</td>
                <td class="auto-style2">
                    &nbsp;</td>
                <td class="auto-style2">
                    &nbsp;</td>
            </tr>
             <tr>
                <td class="auto-style1">
                    <span class="auto-style4">Random</span> <span class="auto-style4">Username</span></td>
                <td class="auto-style2">
                    <asp:Label ID="lbl_user" runat="server"></asp:Label>
                </td>
                <td class="auto-style2">
                    &nbsp;</td>
            </tr>
             <tr>
                <td class="auto-style1">
                    &nbsp;</td>
                <td class="auto-style2">
                    &nbsp;</td>
                <td class="auto-style2">
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">
                    Random Password</td>
                <td>
                    <asp:Label ID="lbl_pass" runat="server"></asp:Label>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
             <tr>
                            <td>&nbsp;</td>
                            <td>
                                </td></tr>
            <tr>
                <td class="auto-style3">
                    &nbsp;</td>
                <td>
    <%--<asp:Button ID="btn_submit" runat="server" Text="Submit" onclick="btn_submit_Click" 
                       CssClass="btn" />--%>
                                <asp:Button ID="btn_add" runat="server" CssClass="btn" OnClick="btn_add_Click" Text="Add" />
                                
                                
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style3">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
                <td>
                    <asp:Button ID ="ButDummy" runat="server" style =" display:none;" Text="Button"/>
                    <cc1:ModalPopupExtender ID="btn_add_ModalPopupExtender" runat="server" DynamicServicePath="" Enabled="True" TargetControlID="btn_add" BackgroundCssClass="modalBackground" RepositionMode="None" PopupControlID="Panel_Popup">
                    </cc1:ModalPopupExtender>

                </td>
            </tr>
         
            <tr>
                <td colspan="2">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
             <tr>
            <td style="width: 400px; height: 150px;"><asp:Panel ID="Panel_Popup" runat="server" Height="135px" BorderStyle="Solid" CssClass="modalPopup" Visible="False" Width="316px">
                                                                    <div>
                                                                        <br />
                                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <asp:Label ID="Lbl_added" runat="server" CssClass="modal-body" Text="Added Successfully!!!" Font-Size="11pt"></asp:Label>
                                                                        <br />
&nbsp;<br /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <asp:Button ID="btn_OK" runat="server" Text="OK" OnClick="btb_OK_Click" CssClass="btnme" />
                                                                        <br />
                                                                        <br />
                                                                        
                                                                        &nbsp;</div>
                                                                </asp:Panel></td>
            <td>
                <asp:ScriptManager ID="ScriptManager1" runat="server">
                </asp:ScriptManager>
            </td>
            <td>&nbsp;</td>
        </tr>
            <tr>
                <td colspan="2">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table></center>
    </form>
</asp:Content>

