﻿//Approve NCD Employee user
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Admin_approveuserncd : System.Web.UI.Page
{
    conclass c = new conclass();
    int empno = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        empno = Convert.ToInt32(Request.QueryString["EmpNo"].ToString());
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Admin/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            BindTextBoxvalues();
        }
    }

    private void BindTextBoxvalues()
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select * from NCDEmployeeDetails c inner join Department d on c.deptid=d.deptid inner join Designation s on c.desigid=s.desigid inner join EmpTypeMaster e on e.emptypeid=c.emptypeid where empid='" + empno + "'", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            DataRow row11 = dt.Rows[dt.Rows.Count - 1];

            String name = Convert.ToString(row11[2]);
            String eid = Convert.ToString(row11[0]);

            String dob = Convert.ToString(row11[3]);
            String email = Convert.ToString(row11[18]);
            String etype = Convert.ToString(row11[27]);
            String gen = Convert.ToString(row11[4]);
            String jdate = Convert.ToString(row11[5]);
            String padd1 = Convert.ToString(row11[6]);
            String padd2 = Convert.ToString(row11[7]);
            String pplace = Convert.ToString(row11[8]);
            String ppin = Convert.ToString(row11[9]);
            String cadd1 = Convert.ToString(row11[10]);
            String cadd2 = Convert.ToString(row11[11]);
            String cplace = Convert.ToString(row11[12]);
            String cpin = Convert.ToString(row11[13]);
            String cnt1 = Convert.ToString(row11[14]);
            String cnt2 = Convert.ToString(row11[15]);
            String dept = Convert.ToString(row11[22]);
            String desig = Convert.ToString(row11[25]);

            Image1.ImageUrl = Convert.ToString(row11[20]);

            txt_empname.Text = name;
            txt_empid.Text = eid;
            txt_dob.Text = dob;
            txt_email.Text = email;
            txt_gen.Text = gen;
            txt_joiningdate.Text = jdate;
            txt_padd1.Text = padd1;
            txt_padd2.Text = padd2;
            txt_pplace.Text = pplace;
            txt_ppin.Text = ppin;
            txt_cadd1.Text = cadd1;
            txt_cadd2.Text = cadd2;
            txt_cplace.Text = cplace;
            txt_cpin.Text = cpin;
            txt_con1.Text = cnt1;
            txt_con2.Text = cnt2;
            txt_dept.Text = dept;
            txt_desigid.Text = desig;
            txt_emptypeid.Text = etype;

        }
    }


    //protected void btn_approve_Click(object sender, EventArgs e)
    //{
    //    c.getCon();

    //    String status = "Approved";
    //    SqlCommand cmd = new SqlCommand("update CasualityEmployeeDetails set status='" + status + "' where empid='" + empno + "'", c.Con);

    //    int result = cmd.ExecuteNonQuery();

    //    if (result == 1)
    //    {
    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowSuccess", "javascript:alert('Employee Approved Successfully');", true);
    //        Response.Write("Employee Approved");
    //    }
    //    Response.Redirect("homepageadmin.aspx");


    //}
    protected void btn_createuser_cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("adduserpagencd.aspx?EmpNo=" + empno);
    }
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("ViewNewEmps.aspx");
    }
}