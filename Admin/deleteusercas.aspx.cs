﻿//Delete casuality Employees
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class Admin_deleteusercas : System.Web.UI.Page
{
    conclass c = new conclass();
    int empno = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        empno = Convert.ToInt32(Request.QueryString["EmpNo"].ToString());
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Admin/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            BindTextBoxvalues();
        }
    }
    private void BindTextBoxvalues()
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select * from CasualityEmployeeDetails where empid='" + empno + "'", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        txt_empid.Text = dt.Rows[0][0].ToString();
        txt_empname.Text = dt.Rows[0][2].ToString();
        txt_cadd1.Text = dt.Rows[0][10].ToString();
        txt_cadd2.Text = dt.Rows[0][11].ToString();
        txt_con1.Text = dt.Rows[0][14].ToString();
        txt_con2.Text = dt.Rows[0][15].ToString();
        txt_cpin.Text = dt.Rows[0][12].ToString();
        txt_cplace.Text = dt.Rows[0][13].ToString();
        txt_dept.Text = dt.Rows[0][16].ToString();
        txt_email.Text = dt.Rows[0][18].ToString();
        txt_desigid.Text = dt.Rows[0][17].ToString();
        txt_emptypeid.Text = dt.Rows[0][1].ToString();
        txt_padd1.Text = dt.Rows[0][6].ToString();
        txt_padd2.Text = dt.Rows[0][7].ToString();
        txt_ppin.Text = dt.Rows[0][8].ToString();
        txt_pplace.Text = dt.Rows[0][9].ToString();
        txt_joiningdate.Text = dt.Rows[0][5].ToString();
        txt_gen.Text = dt.Rows[0][4].ToString();
        txt_dob.Text = dt.Rows[0][3].ToString();
        Image1.ImageUrl = Convert.ToString(dt.Rows[0][20]);

    }
    protected void btn_delete_Click(object sender, EventArgs e)
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("delete from CasualityEmployeeDetails  where empid='" + empno + "'", c.Con);

        int result = cmd.ExecuteNonQuery();

       
        Response.Redirect("~/Admin/homepageadmin.aspx");
        //Response.Write("<script>alert(' Record Deleted Successfully');</script>");
        txt_empid.Text = " ";
        txt_empname.Text = " ";
        txt_cadd1.Text = " ";
        txt_cadd2.Text = " ";
        txt_con1.Text = " ";
        txt_con2.Text = " ";
        txt_cpin.Text = " ";
        txt_cplace.Text = " ";
        txt_dept.Text = " ";
        txt_email.Text = " ";
        txt_desigid.Text = " ";
        txt_emptypeid.Text = " ";
        txt_padd1.Text = " ";
        txt_padd2.Text = " ";
        txt_ppin.Text = " ";
        txt_pplace.Text = " ";
        txt_joiningdate.Text = " ";
        txt_gen.Text = " ";
        txt_dob.Text = " ";


    }
    protected void btn_cancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("homepageadmin.aspx");
    }
}