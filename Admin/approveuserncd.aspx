﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminHomeMaster.master" AutoEventWireup="true" CodeFile="approveuserncd.aspx.cs" Inherits="Admin_approveuserncd" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <title></title>
    <link href="../assets/css/flexslider.css" rel="stylesheet" />
    <style type="text/css">
        .auto-style1 {
            height: 22px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">    <form id="form1" runat="server">
    <center>
    <div>    
       <table>    
           <tr>    
               <td>&nbsp;</td>    
               <td>    
                   &nbsp;</td>    
           </tr>    
           <tr>    
               <td>&nbsp;</td>    
               <td>    
                   <asp:Image ID="Image1" runat="server" Height="200px" Width="200px" />
               </td>    
           </tr>    
           <tr>    
               <td class="auto-style1">Employee Id</td>    
               <td class="auto-style1">    
                   <asp:TextBox ID="txt_empid" runat="server" ReadOnly="true" Enabled="False" />    
               </td>    
           </tr>    
           <tr>    
               <td class="auto-style1">&nbsp;</td>    
               <td class="auto-style1">    
                   &nbsp;</td>    
           </tr>    
           <tr>    
               <td>Employee Type </td>    
               <td>    
                   <asp:TextBox ID="txt_emptypeid" runat="server" ReadOnly="true" Enabled="False"/>    
               </td>    
           </tr>
           <tr>    
               <td>&nbsp;</td>    
               <td>    
                   &nbsp;</td>    
           </tr>
           <tr>    
               <td>Employee Name</td>    
               <td>    
                   <asp:TextBox ID="txt_empname" runat="server" ReadOnly="true" Enabled="False"/>    
               </td>    
           </tr> 
           <tr>    
               <td>&nbsp;</td>    
               <td>    
                   &nbsp;</td>    
           </tr>  
           <tr>    
               <td>Date of Birth</td>    
               <td>    
                   <asp:TextBox ID="txt_dob" runat="server" ReadOnly="true" Enabled="False"/> </td>    
           </tr> 
           <tr>    
               <td>&nbsp;</td>    
               <td>    
                   &nbsp;</td>    
           </tr>  
           <tr>    
               <td>Gender</td>    
               <td>    
                   <asp:TextBox ID="txt_gen" runat="server" ReadOnly="true" Enabled="False"/> </td>    
           </tr>  
           <tr>    
               <td>&nbsp;</td>    
               <td>    
                   &nbsp;</td>    
           </tr> 
           <tr>    
               <td>Joining date</td>    
               <td>    
                   <asp:TextBox ID="txt_joiningdate" runat="server" ReadOnly="true" Enabled="False"/> </td>    
           </tr> 
           <tr>    
               <td>&nbsp;</td>    
               <td>    
                   &nbsp;</td>    
           </tr>  
           <tr><td>Permanent Address:</td></tr> 
           <tr>    
               <td>Address Line 1</td>    
               <td>    
                   <asp:TextBox ID="txt_padd1" runat="server" ReadOnly="true" Enabled="False"/>    
               </td>    
           </tr> 
           <tr>    
               <td>&nbsp;</td>    
               <td>    
                   &nbsp;</td>    
           </tr>   
           <tr>    
               <td>Address Line 2</td>    
               <td>    
                   <asp:TextBox ID="txt_padd2" runat="server" ReadOnly="true" Enabled="False"/>    
               </td>    
           </tr> 
           <tr>    
               <td>&nbsp;</td>    
               <td>    
                   &nbsp;</td>    
           </tr>
           <tr>    
               <td>Place</td>    
               <td>    
                   <asp:TextBox ID="txt_pplace" runat="server" ReadOnly="true" Enabled="False" />    
               </td>    
           </tr> 
           <tr>    
               <td>&nbsp;</td>    
               <td>    
                   &nbsp;</td>    
           </tr>
           <tr>    
               <td>Pin</td>    
               <td>    
                   <asp:TextBox ID="txt_ppin" runat="server"  ReadOnly="true" Enabled="False"/>    
               </td>    
           </tr> <tr>    
               <td>&nbsp;</td>    
               <td>    
                   &nbsp;</td>    
           </tr>   
           <tr><td>Current Address:</td></tr> 
           <tr>    
               <td>Address Line 1</td>    
               <td>    
                   <asp:TextBox ID="txt_cadd1" runat="server" ReadOnly="true" Enabled="False"/>    
               </td>    
           </tr>   <tr>    
               <td>&nbsp;</td>    
               <td>    
                   &nbsp;</td>    
           </tr> 
           <tr>    
               <td>Address Line 2</td>    
               <td>    
                   <asp:TextBox ID="txt_cadd2" runat="server" ReadOnly="true" Enabled="False"/>    
               </td>    
           </tr> <tr>    
               <td>&nbsp;</td>    
               <td>    
                   &nbsp;</td>    
           </tr>
           <tr>    
               <td>Place</td>    
               <td>    
                   <asp:TextBox ID="txt_cplace" runat="server" ReadOnly="true" Enabled="False" />    
               </td>    
           </tr> <tr>    
               <td>&nbsp;</td>    
               <td>    
                   &nbsp;</td>    
           </tr>
           <tr>    
               <td>Pin</td>    
               <td>    
                   <asp:TextBox ID="txt_cpin" runat="server" ReadOnly="true" Enabled="False"/>    
               </td>    
           </tr>  <tr>    
               <td>&nbsp;</td>    
               <td>    
                   &nbsp;</td>    
           </tr>  
           <tr>    
               <td>Contact 1</td>    
               <td>    
                   <asp:TextBox ID="txt_con1" runat="server"  ReadOnly="true" Enabled="False"/>    
               </td>    
           </tr><tr>    
               <td>&nbsp;</td>    
               <td>    
                   &nbsp;</td>    
           </tr>
           <tr>    
               <td>Contact 2</td>    
               <td>    
                   <asp:TextBox ID="txt_con2" runat="server"  ReadOnly="true" Enabled="False" />    
               </td>    
           </tr> <tr>    
               <td>&nbsp;</td>    
               <td>    
                   &nbsp;</td>    
           </tr>
           <tr>    
               <td>Email Address</td>    
               <td>    
                   <asp:TextBox ID="txt_email" runat="server"  ReadOnly="true" Enabled="False"/>   </td>    
           </tr> <tr>    
               <td>&nbsp;</td>    
               <td>    
                   &nbsp;</td>    
           </tr>
           <tr>    
               <td>Department </td>    
               <td>    
                   <asp:TextBox ID="txt_dept" runat="server"  ReadOnly="true" Enabled="False" />    
               </td>    
           </tr> <tr>    
               <td>&nbsp;</td>    
               <td>    
                   &nbsp;</td>    
           </tr>
           <tr>    
               <td>Designation </td>    
               <td>    
                   <asp:TextBox ID="txt_desigid" runat="server"  ReadOnly="true" Enabled="False"/>    
               </td>    
           </tr>    
           <tr>    
               <td align="center">    
                    &nbsp;</td>   
               <td align="center">    
                   &nbsp;</td>  
               <td align="center">    
                   &nbsp;</td>    
           </tr>    
           <tr>    
               <td align="center">    
                   <asp:Button ID="btn_createuser" runat="server" Text="Approve and Create User" OnClick="btn_createuser_cancel_Click" CssClass="btn" />    
               </td>   
               <td align="center">    
                   <asp:Button ID="btn_cancel" runat="server" Text="Cancel" OnClick="btn_cancel_Click" CssClass="btn" />    
               </td>  
               <td align="center">    
                   &nbsp;</td>    
           </tr>    
           <tr>    
               <td align="center">    
                    &nbsp;</td>   
               <td align="center">    
                   &nbsp;</td>  
               <td align="center">    
                   &nbsp;</td>    
           </tr>    
           <tr>    
               <td align="center">    
                    &nbsp;</td>   
               <td align="center">    
                   &nbsp;</td>  
               <td align="center">    
                   &nbsp;</td>    
           </tr>    
       </table>    
    </div>    
            </center>

    </form>
</asp:Content>



