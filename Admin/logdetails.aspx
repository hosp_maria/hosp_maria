﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin/AdminHomeMaster.master" AutoEventWireup="true" CodeFile="logdetails.aspx.cs" Inherits="Admin_logdetails" %>

 <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="style.css" rel="stylesheet" />

    <script language="javascript" type="text/javascript">
        function CallPrint(strid) {
            var prtContent = document.getElementById(strid);
            var WinPrint = window.open('', '', 'letf=0,top=0,width=800,height=100,toolbar=0,scrollbars=0,status=0,dir=ltr');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
            prtContent.innerHTML = strOldOne;

        }
</script>

  

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <table class="auto-style2">
             <tr>
                <td ><ajax:ToolkitScriptManager ID="toolkit1" runat="server"></ajax:ToolkitScriptManager></td>
                <td class="auto-style12" >&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:MultiView ID="MultiView1" runat="server">
                        <table class="auto-style2">
                           
            <tr>
                            <td>
                                <asp:View ID="View1" runat="server">
                                    <section class= "featured-service-content">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 service">
						<div class="row text-center">
							<div class="service-icon text-center">
		        				<i class="fa fa-5x fa-male"></i>
		        			</div>
						</div>
		        			
	        			<div class="row text-center">
	        				<div class="about-service">
		        				<h3 class="text-center"><asp:LinkButton ID="link_user" runat="server" OnClick="link_user_Click">Search by User</asp:LinkButton></h3>
		        				<%--<p class="text-center">
		        					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		                       		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		                       		quis nostrud 
		        				</p>--%>
		        			</div>
	        			</div>
		 						
	        		</div>	<!-- col-sm-4 -->

	        		<div class="col-sm-4 service">
	        			<div class="row text-center">
	        				<div class="service-icon text-center">
		        				<i class="fa fa-calendar fa-5x"></i>
		        			</div>
	        			</div>
		        			
	        			<div class="row text-center">
	        				<div class="about-service">
		        				<h3 class="text-center"> <asp:LinkButton ID="link_time" runat="server" OnClick="link_time_Click1">Search by Time</asp:LinkButton></h3>
		        				<%--<p class="text-center">
		        					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
		                       		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
		                       		quis nostrud 
		        				</p>--%>
		        			</div>
	        			</div>
		        			
	        		</div>	<!-- col-sm-4 -->
	   
                                </asp:View>
                            </td>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:View ID="View2" runat="server">
                                        <table class="auto-style2">
                                            <tr>
                                                <td class="auto-style5">UserName</td>
                                                <td class="auto-style3">
                                                    <asp:TextBox ID="txt_user" runat="server"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_user" ErrorMessage="*Enter Username" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style4">From Date (mm/dd/yyyy)</td>
                                                <td>
                                                    <asp:TextBox ID="txt_date" runat="server"></asp:TextBox>
                                                    <ajax:CalendarExtender ID="CalendarExtender1" runat="server" Format="MM/dd/yyyy" TargetControlID="txt_date">
                                                    </ajax:CalendarExtender>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_date" ErrorMessage="*Enter Date " ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>
                                                    <asp:Button ID="btn_srch" runat="server" OnClick="btn_srch_Click" Text="Search" CssClass="btn" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:View ID="View3" runat="server">
                                        <table class="auto-style2">
                                            <tr>
                                                <td class="auto-style4">From Date (mm/dd/yyyy)</td>
                                                <td>
                                                    <asp:TextBox ID="txt_from" runat="server"></asp:TextBox>
                                                    <ajax:CalendarExtender ID="CalendarExtender2" runat="server" Format="MM/dd/yyyy" TargetControlID="txt_from">
                                                    </ajax:CalendarExtender>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txt_from" ErrorMessage="*Enter Date " ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style4">To Date (mm/dd/yyyy)</td>
                                                <td>
                                                    <asp:TextBox ID="txt_to" runat="server"></asp:TextBox>
                                                    <ajax:CalendarExtender ID="CalendarExtender3" runat="server" Format="MM/dd/yyyy" TargetControlID="txt_to">
                                                    </ajax:CalendarExtender>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txt_to" ErrorMessage="*Enter Date " ForeColor="#CC0000"></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>
                                                    <asp:Button ID="btn_srch2" runat="server" OnClick="btn_srch2_Click" Text="Search" CssClass="btn" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:View>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style3">
                                    <asp:View ID="View4" runat="server">
                                        <div id="print">
                                            <table class="auto-style2">
                                                <tr>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td><center><b>Government General Hospital,Changanacherry</b></center></td>
                                                </tr>
                                                 <tr>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                                 <tr>
                                                    <td>
                                                        <center><b>User Log Details</b></center></td>
                                                </tr>
                                                 <tr>
                                                    <td>
                                                        &nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:GridView ID="gridview_emp" runat="server" AutoGenerateColumns="False" CellPadding="3" CssClass="auto-style2" DataKeyNames="empid" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White" Width="424px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                                                            <Columns>
                                                                <asp:BoundField DataField="logid" HeaderText="Log Id" />
                                                                <asp:BoundField DataField="empid" HeaderText="Employee Id" />
                                                                <asp:BoundField DataField="username" HeaderText="Username" />
                                                                <asp:BoundField DataField="module" HeaderText="Module" />
                                                               <asp:BoundField DataField="datentime" HeaderText="Date and Time of Login" />
                                                            </Columns>
                                                            <FooterStyle BackColor="White" ForeColor="#000066" />
                                                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                            <RowStyle ForeColor="#000066" />
                                                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                        </asp:GridView>
                                                        &nbsp;<asp:Label ID="lbl_no" runat="server" ForeColor="#CC0000" Text="No Log details found"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr></table></div><table>
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="BtnPrint" runat="server" onclientclick="javascript:CallPrint('print');" text="Print" Width="103px" xmlns:asp="#unknown" CssClass="btn" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            </table>
                                        
                                    </asp:View>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </asp:MultiView>
                </td>
            </tr>
        </table>
    </form>
</asp:Content>