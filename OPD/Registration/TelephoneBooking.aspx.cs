﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class OPD_Registration_TelephoneBooking : System.Web.UI.Page
{
    conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/OPD/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
            lbl_doc.Visible = false;
            lbl_seldoc.Visible = false;
            ddl_doc.Items.Insert(0, "[SELECT]");
            ddl_dept.Items.Insert(0, "[SELECT]");
            c.getCon();
            SqlCommand cmddept = new SqlCommand("select * from Department d inner join EmpTypeMaster e on e.emptypeid=d.emptype where e.emptypename like 'D%' and deptname not in('NCD Clinic','Casuality')  order by deptname desc", c.Con);
            SqlDataAdapter sdadept = new SqlDataAdapter(cmddept);
            DataTable dtdept = new DataTable();
            sdadept.Fill(dtdept);

            int idept = cmddept.ExecuteNonQuery();
            int j = dtdept.Rows.Count;
            if (j > 0)
            {
                ddl_dept.DataSource = dtdept;
                ddl_dept.DataValueField = "deptid";
                ddl_dept.DataTextField = "deptname";
                ddl_dept.DataBind();
                ddl_dept.Items.Insert(0, "[SELECT]");

            }
            c.Con.Close();
          
        }
    }
    protected void btn_search_Click(object sender, EventArgs e)
    {
        c.getCon();
        SqlCommand cmdbk = new SqlCommand("select * from patientdetails where patientid= '" + txt_pid.Text + "' ", c.Con);
        SqlDataAdapter sdabk = new SqlDataAdapter(cmdbk);
        DataTable dtbk = new DataTable();
        sdabk.Fill(dtbk);

        int k = cmdbk.ExecuteNonQuery();

        if (dtbk.Rows.Count > 0)
        {
            DataRow rowbk = dtbk.Rows[dtbk.Rows.Count - 1];
            DateTime todaydate = DateTime.Now;
            string pname = Convert.ToString(rowbk[1]);
            String pdob = Convert.ToString(rowbk[7]);
            DateTime lrdate = Convert.ToDateTime(rowbk[9]);
            int y = todaydate.Year.CompareTo(lrdate.Year);
            if (y >= 1)
            {
                Response.Write("<script>alert('Renewal Needed');</script>");
            }

            txt_pname.Text = pname;
            txt_dob.Text = pdob;
        }
        c.Con.Close();
    
    }
    protected void ddl_dept_SelectedIndexChanged(object sender, EventArgs e)
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select * from EmployeeDetails  where emptypeid=5 and deptid='" + ddl_dept.SelectedItem.Value + "' order by empname desc", c.Con);
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        sda.Fill(dt);

        int i = cmd.ExecuteNonQuery();
        int j = dt.Rows.Count;

        if (j > 0)
        {
            ddl_doc.DataSource = dt;
            ddl_doc.DataValueField = "empid";
            ddl_doc.DataTextField = "empname";
            ddl_doc.DataBind();
            ddl_doc.Items.Insert(0, "[SELECT]");
            ddl_doc.Items.Insert(1, "No preference");

        }
        c.Con.Close();
    }
    protected void btn_generate_Click(object sender, EventArgs e)
    {
        int count;
        c.getCon();
        if (ddl_dept.SelectedIndex != 0)
        {
            if (ddl_doc.SelectedIndex != 0)
            {
        if (ddl_doc.SelectedItem.Text == "No preference")
            ddl_doc.SelectedItem.Value = "0";
        SqlCommand cmd = new SqlCommand("select count(*) from Schedule where scheduledate='" + txtDate.Text + "' and deptid ='" + ddl_dept.SelectedItem.Value + "'", c.Con);
        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        sda.Fill(dt);

        int r = cmd.ExecuteNonQuery();

        if (dt.Rows.Count > 0)
        {
            DataRow ro = dt.Rows[dt.Rows.Count - 1];
            count = Convert.ToInt32(ro[0]);
            lbl_token.Text = Convert.ToString(count + 1);
        }
        String str = "insert into BookingTable values('"+txtDate.Text+"','"+DateTime.Today+"', '" + txt_pid.Text + "','" + ddl_dept.SelectedItem.Value + "','" + ddl_doc.SelectedItem.Value + "','"+lbl_token.Text+"')";
        SqlCommand cmds = new SqlCommand(str, c.Con);
        cmds.ExecuteNonQuery();

        String s = "insert into Schedule values('" + txt_pid.Text + "','" + txtDate.Text + "','" + DateTime.Now + "','" + ddl_dept.SelectedItem.Value + "','Confirmed','" + ddl_doc.SelectedItem.Value + "',' ')";
        SqlCommand cmd2 = new SqlCommand(s, c.Con);
        cmd2.ExecuteNonQuery();
        txt_dob.Text = " ";
        txt_pid.Text = " ";
        txt_pname.Text = " ";
        ddl_dept.SelectedIndex = 0;
        ddl_doc.SelectedIndex = 0;
        txtDate.Text = " ";
            }
            else
            {
                lbl_seldoc.Visible = true;
            }
        }
        else
        {
            lbl_doc.Visible = true;
        }
        c.Con.Close();
        
    }
}