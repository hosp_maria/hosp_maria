﻿
<%@ Page Title="" Language="C#" MasterPageFile="~/OPD/Registration/Regmaster.master" AutoEventWireup="true" CodeFile="generatetoken.aspx.cs" Inherits="Registration_generatetoken" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
     <%--<script language="javascript" type="text/javascript">
        function CallPrint(strid) {
            var prtContent = document.getElementById(strid);
            var WinPrint = window.open('', '', 'letf=0,top=0,width=1,height=1,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
            prtContent.innerHTML = strOldOne;
        }
</script>--%>
        <link href="style.css" rel="stylesheet" />

     

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <table >
            <tr >
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr >
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td >Enter Patient ID</td>
                <td >
                    <asp:TextBox ID="txt_pid" runat="server" CssClass="twitter"></asp:TextBox>
                    
                    <asp:Button ID="btn_search" runat="server" OnClick="btn_search_Click" Text="Search" CssClass="btn"  />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_pid" ErrorMessage="*Enter Card Number" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                </td>
            </tr>
             <tr>
                    <td > &nbsp;</td>
                    <td > &nbsp;</td>
                        </tr>
            <tr>
                <td >Patient Name</td>
                <td >
                    <asp:TextBox ID="txt_pname" runat="server" ReadOnly="True" Enabled="False" CssClass="twitter"></asp:TextBox>
                </td>
            </tr>
             <tr>
                    <td > &nbsp;</td>
                    <td > &nbsp;</td>
                        </tr>
            <tr>
                <td >Patient Date of Birth</td>
                <td >
                    <asp:TextBox ID="txt_dob" runat="server" Enabled="False" ReadOnly="True" CssClass="twitter"></asp:TextBox>
                </td>
            </tr>
             <tr>
                    <td > &nbsp;</td>
                    <td > &nbsp;</td>
                        </tr>
            <tr>
                <td >Select Doctor Department&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td >
                    <asp:DropDownList ID="ddl_dept" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddl_dept_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:Label ID="lbl_doc" runat="server" ForeColor="#CC0000" Text="*Select Department"></asp:Label>
                </td>
            </tr>
             <tr>
                    <td > &nbsp;</td>
                    <td > &nbsp;</td>
                        </tr>
            <tr>
                <td >Select Doctor</td>
                <td >
                    <asp:DropDownList ID="ddl_doc" runat="server" >
                    </asp:DropDownList>
                    <asp:Label ID="lbl_seldoc" runat="server" ForeColor="#CC0000" Text="*Select Doctor"></asp:Label>
                </td>
            </tr>
             <tr>
                    <td > &nbsp;</td>
                    <td > &nbsp;</td>
                        </tr>
            <tr>
                <td >
                    <asp:Button ID="btn_generate" runat="server" OnClick="btn_generate_Click" Text="Generate" CssClass="btn" />
                </td>
                <td >
                    &nbsp;</td>
            </tr>
            <tr >
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            </table>
            <div id="bill">
                <table id="tab_print" runat="server" style="font-size: medium; width: 640px;"> 
                    <tr>
                    <td > &nbsp;</td>
                    <td> &nbsp;</td>
                        </tr>
                    <tr>
                    <td >
                        <asp:Image ID="img_logo" runat="server" height="100" Width="100" ImageUrl="~/OPD/Registration/image/gov.png"/></td>
                                        <td >Government General Hospital
                                            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Changanacherry</td>    </tr>
                     <tr>
                    <td > &nbsp;</td>
                    <td > &nbsp;</td>
                        </tr>
                     <tr>
                    <td >Date:</td>
                    <td >
                        <asp:Label ID="lbl_date" runat="server"></asp:Label></td>
                        </tr>
                     <tr>
                    <td > &nbsp;</td>
                    <td > &nbsp;</td>
                        </tr>
                     <tr>
                    <td >Patient No:</td>
                    <td >
                        <asp:Label ID="lbl_patno" runat="server"></asp:Label></td>
                        </tr>
                     <tr>
                    <td > &nbsp;</td>
                    <td > &nbsp;</td>
                        </tr>
                     <tr>
                     <td>Patient Name:</td>
                    <td >
                        <asp:Label ID="lbl_pname" runat="server"></asp:Label></td>
                        </tr>
                     <tr>
                    <td > &nbsp;</td>
                    <td > &nbsp;</td>
                        </tr>
                      <tr>
                    <td >Age:</td>
                    <td >
                        <asp:Label ID="lbl_age" runat="server"></asp:Label></td>
                        </tr>
                     <tr>
                    <td > &nbsp;</td>
                    <td > &nbsp;</td>
                        </tr>
                      <tr>
                    <td >Token No:</td>
                    <td >
                        <asp:Label ID="lbl_token" runat="server"></asp:Label></td>
                        </tr>
                     <tr>
                    <td > &nbsp;</td>
                    <td > &nbsp;</td>
                        </tr>
                      <tr>
                    <td >Department:</td>
                    <td >
                        <asp:Label ID="lbl_dept" runat="server"></asp:Label></td>
                        </tr>
                     <tr>
                    <td > &nbsp;</td>
                    <td > &nbsp;</td>
                        </tr>
                      <tr>
                    <td>Doctor:</td>
                    <td >
                        <asp:Label ID="lbl_docname" runat="server"></asp:Label></td>
                        </tr>
                     <tr>
                    <td > &nbsp;</td>
                    <td > &nbsp;</td>
                        </tr>
                    <tr>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                        <td >&nbsp;&nbsp;&nbsp;</td>
                    <td > Posted By: </td>
                       <td> <asp:Label ID="lbl_postedby" runat="server"></asp:Label></td>
                        </tr>
                  
</table>
                </div>
        <p><asp:Button ID="btn_print" runat="server" Text="Print" Width="168px" onclientclick="javascript:CallPrint('bill');" xmlns:asp="#unknown" CssClass="btn" OnClick="btn_print_Click" /></p>
    <div>
    <br /><br />
    </div>
    </form>
</asp:Content>
