﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OPD/Registration/Regmaster.master" AutoEventWireup="true" CodeFile="TelephoneBooking.aspx.cs" Inherits="OPD_Registration_TelephoneBooking" %>
 <%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
    <table  runat="server">
        <tr>
            <td> <ajax:ToolkitScriptManager ID="toolkit1" runat="server"></ajax:ToolkitScriptManager></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
         <tr>
                <td>Enter Patient ID</td>
                <td>
                    <asp:TextBox ID="txt_pid" runat="server" CssClass="twitter"></asp:TextBox>
                                        <asp:Button ID="btn_search" runat="server" OnClick="btn_search_Click" Text="Search" CssClass="btn" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_pid" ErrorMessage="*Enter Patient Card no" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                </td>
            </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
            <tr>
                <td >Patient Name</td>
                <td>
                    <asp:TextBox ID="txt_pname" runat="server" ReadOnly="True" Enabled="False" CssClass="twitter"></asp:TextBox>
                </td>
            </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
            <tr>
                <td>Patient Date of Birth</td>
                <td>
                    <asp:TextBox ID="txt_dob" runat="server" Enabled="False" ReadOnly="True" CssClass="twitter"></asp:TextBox>
                </td>
            </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
            <tr>
                <td >Select Doctor Department    </td>
                <td >
                    <asp:DropDownList ID="ddl_dept" runat="server" CssClass="twitter" AutoPostBack="True" OnSelectedIndexChanged="ddl_dept_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:Label ID="lbl_doc" runat="server" ForeColor="#CC0000" Text="*Select Department"></asp:Label>
                </td>
            </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
            <tr>
                <td>Select Doctor</td>
                <td>
                    <asp:DropDownList ID="ddl_doc" runat="server" CssClass="twitter" >
                    </asp:DropDownList>
                    <asp:Label ID="lbl_seldoc" runat="server" ForeColor="#CC0000" Text="*Select Doctor"></asp:Label>
                </td>
            </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
            <tr>
                <td>
                    Date of Booking</td>
                <td>
                     <asp:TextBox ID="txtDate" runat="server" CssClass="twitter" />
<ajax:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDate" Format="MM/dd/yyyy" runat="server">
</ajax:CalendarExtender>
                </td>
            </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
            <tr>
                <td>
                    <asp:Button ID="btn_generate" runat="server" OnClick="btn_generate_Click" Text="Book appointment" CssClass="btn" />
                </td>
                <td>&nbsp;</td>
            </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
                    <td >Token No:</td>
                    <td >
                        <asp:Label ID="lbl_token" runat="server"></asp:Label></td>
                        </tr>
          
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
    </form>
</asp:Content>

