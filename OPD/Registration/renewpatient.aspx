﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OPD/Registration/Regmaster.master" AutoEventWireup="true" CodeFile="renewpatient.aspx.cs" Inherits="Registration_renewpatient" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <title></title>
    
                    <link href="style.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
    <div>
    
        <table >
            <tr>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
                        <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Enter Patient No:</td>
                <td>
                    <asp:TextBox ID="txt_pno" runat="server" CssClass="twitter"></asp:TextBox>
                    <asp:Button ID="btn_search" runat="server" CssClass="btn" OnClick="btn_search_Click" Text="Search" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_pno" ErrorMessage="*Enter Patient Card no" ForeColor="#CC0000"></asp:RequiredFieldValidator>
                </td>
            </tr>
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Patient Name:</td>
                <td>
                    <asp:Label ID="lbl_pname" runat="server"></asp:Label>
                </td>
            </tr>
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Date of Birth:</td>
                <td >
                    <asp:Label ID="lbl_dob" runat="server"></asp:Label>
                </td>
            </tr>
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr >
                <td>Date of Last Renewal&nbsp;&nbsp;&nbsp;&nbsp; </td>
                <td>
                    <asp:Label ID="lbl_lrd" runat="server"></asp:Label>
                </td>
            </tr>
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Guardian Name:</td>
                <td>
                    <asp:Label ID="lbl_gname" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btn_renew" runat="server" CssClass="btn" OnClick="btn_renew_Click" Text="Renew" />
                </td>
                <td>
                    <asp:Button ID="btn_cancel" runat="server" CssClass="btn" Text="Cancel"  OnClick="btn_cancel_Click" /></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
             <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</asp:Content>

