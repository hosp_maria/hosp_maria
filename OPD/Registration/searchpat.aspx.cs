﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class OPD_Registration_searchpat : System.Web.UI.Page
{
    conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
            if (Session["Id"] == null)
                Response.Redirect("~/OPD/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
        }

    }

    protected void gridview_pat_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = gridview_pat.Rows[index];
            Response.Redirect("printregisteredpatients.aspx?PatientNo=" + row.Cells[0].Text);
        }
    }
    protected void btn_search_Click(object sender, EventArgs e)
    {
        BindGridView();
    }
    private void BindGridView()
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select * from patientdetails where pname like '%'+'" + txt_name.Text + "'+'%'", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            gridview_pat.DataSource = dt;
            gridview_pat.DataBind();
        }
    }
}