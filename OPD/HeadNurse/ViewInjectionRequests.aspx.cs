﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class HeadNurse_ViewInjectionRequests : System.Web.UI.Page
{
    conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/OPD/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            BindGridView();
            alertstock();
        }
    }
    private void BindGridView()
    {
        c.getCon();
         SqlCommand cmd_pat = new SqlCommand("select deptid from Department where deptname not in ('Casuality','CASUALITY','casuality') ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();
        int j=dt_pat.Rows.Count;
        while (j > 0)
        {
            DataRow row_pat = dt_pat.Rows[j - 1];

            int deptid = Convert.ToInt32(row_pat[0]);
            SqlCommand cmd = new SqlCommand("select * from InjectionTable i inner Join InjectionMaster m on i.injectionid=m.injectionid inner join patientdetails p on p.patientid=i.patientid inner join EmployeeDetails e on i.docid=e.empid where (i.status='Requested' and i.deptid='" + deptid + "' ) or ( i.status='In Progress' and i.deptid='" + deptid + "' )  order by i.injecreqid", c.Con);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                GridView_InjReq.DataSource = dt;
                GridView_InjReq.DataBind();
            }
            j--;
        }
        c.Con.Close();
    }
    protected void GridView_InjReq_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_InjReq.PageIndex = e.NewPageIndex;
        BindGridView();
    }
    protected void GridView_InjReq_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView_InjReq.Rows[index];
            Response.Redirect("InjectionPage.aspx?InjReqNo=" + row.Cells[0].Text);
        }
    }
    public void alertstock()
    {

        c.getCon();
        SqlCommand cmd = new SqlCommand("select s.injectionid,s.stockvalue,i.injectionname from InjectionStock s inner join InjectionMaster i on s.injectionid=i.injectionid ", c.Con);

        SqlDataAdapter sda = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        sda.Fill(dt);

        int i = cmd.ExecuteNonQuery();

        if (dt.Rows.Count > 0)
        {
            DataRow row1 = dt.Rows[dt.Rows.Count - 1];

            int stock = Convert.ToInt32(row1["stockvalue"]);
            int injid = Convert.ToInt32(row1["injectionid"]);
            string injname = Convert.ToString(row1["injectionname"]);
            SqlCommand cmd_count = new SqlCommand("select *  from InjectionOrder where injectionid= '" + injid+ "' and status='Pending' ", c.Con);
            SqlDataAdapter sda_count = new SqlDataAdapter(cmd_count);
            DataTable dt_count = new DataTable();
            sda_count.Fill(dt_count);
            int k_pat = cmd_count.ExecuteNonQuery();
            if (dt_count.Rows.Count == 0)
            {
                if (stock <= 80)
                {
                    string msg = "Stock Refill needed for";
                    ClientScript.RegisterStartupScript(this.GetType(), msg, "<script type='text/javascript'>alert('" + msg + "'+'" + injname + "');window.location='StockRefill.aspx';</script>'");

                }
            }
        }
        c.Con.Close();
    }
}