﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OPD/HeadNurse/HeadNurseMaster.master" AutoEventWireup="true" CodeFile="InjectionPage.aspx.cs" Inherits="HeadNurse_InjectionPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <table frame="border">
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Patient Name</td>
                <td>
                    <asp:Label ID="lbl_patname" runat="server"></asp:Label>
                </td>
            </tr>
             <tr>
                <td >&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Injection Name</td>
                <td>
                    <asp:Label ID="lbl_injname" runat="server"></asp:Label>
                </td>
            </tr>
             <tr>
                <td >&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dosage</td>
                <td>
                    <asp:Label ID="lbl_dosage" runat="server"></asp:Label>
                &nbsp;</td>
            </tr>
             <tr>
                <td >&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Total Number of Doses</td>
                <td>
                    <asp:Label ID="lbl_totaldose" runat="server"></asp:Label>
                </td>
            </tr>
             <tr>
                <td >&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Status</td>
                <td>
                    <asp:Label ID="lbl_stat" runat="server"></asp:Label>
                </td>
            </tr>
             <tr>
                <td >&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Number of Doses taken</td>
                <td>
                    <asp:Label ID="lbl_takenno" runat="server"></asp:Label>
                </td>
            </tr>
             <tr>
                <td >&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Injection requested By</td>
                <td>
                    <asp:Label ID="lbl_reqby" runat="server"></asp:Label>
                </td>
            </tr>
             <tr>
                <td >&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Injection request Date</td>
                <td>
                    <asp:Label ID="lbl_reqdate" runat="server"></asp:Label>
                </td>
            </tr>
             <tr>
                <td >&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Date of Last Dose Taken</td>
                <td>
                    <asp:Label ID="lbl_lastdate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                    <asp:LinkButton ID="link_dose" runat="server" OnClick="link_dose_Click">Dose Taken</asp:LinkButton>
&nbsp;(click after each dose)</td>
                <td >
                    &nbsp;</td>
            </tr>
             <tr>
                <td >&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="GridView_InjRes" runat="server" AutoGenerateColumns="False" CellPadding="3" Width="380px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle ForeColor="#000066" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                        <Columns>
                                 
                                 <asp:BoundField DataField="empname" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Administered By" />
                                <asp:BoundField DataField="dateofinj" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Last Injection date" />

                                </Columns>

                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>
</asp:Content>



