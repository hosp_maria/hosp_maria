﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class HeadNurse_TallyOrder : System.Web.UI.Page
{
    conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/OPD/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            BindGridView();
        }
    }
    public void BindGridView()
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select * from InjectionOrder i inner join InjectionMaster m on i.injectionid=m.injectionid where i.status='Pending'", c.Con);
       DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        c.Con.Close();
    }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGridView();
    
    }
    protected void GridView1_RowCommand1(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView1.Rows[index];
            
            c.getCon();
             SqlCommand cmd = new SqlCommand("select m.stockvalue from InjectionOrder i inner join InjectionStock m on i.injectionid=m.injectionid where i.orderid='" + Convert.ToInt32(row.Cells[0].Text) + "'", c.Con);
       DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            DataRow rows = dt.Rows[dt.Rows.Count - 1];
            String stk = Convert.ToString(rows[0]);
            string upd = "update InjectionOrder set status='Fullfilled',dateoffulfil='" + DateTime.Today + "' where orderid='" + Convert.ToInt32(row.Cells[0].Text) + "'";
            SqlCommand cmds = new SqlCommand(upd, c.Con);
            cmds.ExecuteNonQuery();
            int stock = Convert.ToInt32(row.Cells[2].Text) + Convert.ToInt32(stk);
            string update = "update s set s.stockvalue='" + stock + "',s.openingstock='" +stock + "' from  InjectionStock s inner join InjectionOrder o on o.injectionid=s.injectionid   where o.orderid='" + Convert.ToInt32(row.Cells[0].Text) + "'";
            SqlCommand cmdup = new SqlCommand(update, c.Con);
            cmdup.ExecuteNonQuery();
            GridView1.EditIndex = -1;
            c.Con.Close();
            BindGridView();
        }
        }
    
    }
}