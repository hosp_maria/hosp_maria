﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class HeadNurse_TakeVitals : System.Web.UI.Page
{
        conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindddl();
            if (Session["Id"] == null)
                Response.Redirect("~/OPD/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
        }
    }
    public void bindddl()
    {
        c.getCon();
        SqlCommand cmd_vid = new SqlCommand("select * from VitalMaster ", c.Con);
        SqlDataAdapter sda_vid = new SqlDataAdapter(cmd_vid);
        DataTable dt_vid = new DataTable();
        sda_vid.Fill(dt_vid);


        int k_vid = cmd_vid.ExecuteNonQuery();

        if (dt_vid.Rows.Count > 0)
        {
            ddl_vital.DataSource = dt_vid;
            ddl_vital.DataTextField = "vital";
            ddl_vital.DataValueField = "vitalmasterid";
            ddl_vital.DataBind();
        }
        ddl_vital.Items.Insert(0, "[Select]");
        c.Con.Close();
    }

    
    protected void btn_search_Click(object sender, EventArgs e)
    {
        c.getCon();
        int pno = Convert.ToInt32(txt_cardno.Text);
        SqlCommand cmd_pat = new SqlCommand("select * from patientdetails where patientid= '" + pno + "' ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];

            String name = Convert.ToString(row_pat[1]);
            String pid = Convert.ToString(row_pat[0]);

            DateTime dob = Convert.ToDateTime(row_pat[7]);
            String gender = Convert.ToString(row_pat[14]);
            int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;
            lbl_dob.Text = Convert.ToString(age) + " years";
            lbl_gender.Text = gender;
            lbl_name.Text = name;
        }
        c.Con.Close();
        BindGridView_Vitals();

    }
    protected void btn_add_Click(object sender, EventArgs e)
    {
        c.getCon();
        int pno = Convert.ToInt32(txt_cardno.Text);
        //SqlCommand cmd_vid = new SqlCommand("select * from VitalMaster where vital='"+txt_particulars.Text+"' ", c.Con);
        //SqlDataAdapter sda_vid = new SqlDataAdapter(cmd_vid);
        //DataTable dt_vid = new DataTable();
        //sda_vid.Fill(dt_vid);


        //int k_vid = cmd_vid.ExecuteNonQuery();

        //if (dt_vid.Rows.Count > 0)
        //{
        //    DataRow row_vid = dt_vid.Rows[dt_vid.Rows.Count - 1];

        //    int vid = Convert.ToInt32(row_vid[0]);
            String s = "insert into VitalTable values('" + txt_value.Text + "','" + txt_remarks.Text + "','" + DateTime.Today + "','" + txt_cardno.Text + "','" + Session["uid"] + "','" + ddl_vital.SelectedItem.Value + "')";
            SqlCommand cmds = new SqlCommand(s, c.Con);
            cmds.ExecuteNonQuery();
        //}
        c.Con.Close();
        txt_remarks.Text = " ";
        txt_value.Text = " ";
        //txt_particulars.Text = " ";
        
        BindGridView_Vitals();
    }
    public void BindGridView_Vitals()
    {
        c.getCon();
        int pno = Convert.ToInt32(txt_cardno.Text);

        SqlCommand cmd = new SqlCommand("select * from VitalMaster vm inner join VitalTable v on vm.vitalmasterid=v.vitalmasterid inner join EmployeeDetails e on e.empid=v.nurseid  where patientid=@pno and nurseid=@nurse and dateofvital=@date order by vitalid desc", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);
        cmd.Parameters.AddWithValue("@nurse", Session["uid"]);
        cmd.Parameters.AddWithValue("@date", DateTime.Today);

        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_Vitals.DataSource = dt;
            GridView_Vitals.DataBind();
        }
        c.Con.Close();

    }
    protected void GridView_Vitals_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_Vitals.PageIndex = e.NewPageIndex;
        BindGridView_Vitals();
    }
}