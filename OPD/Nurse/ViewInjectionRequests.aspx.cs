﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

public partial class Nurse_ViewInjectionRequests : System.Web.UI.Page
{
    conclass c = new conclass();
  
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/OPD/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            BindGridView();
        }
    }
    private void BindGridView()
    {
        c.getCon();
        SqlCommand cmd_pat = new SqlCommand("select deptid from Department where deptname not in ('Casuality','CASUALITY','casuality') ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();
        int j=dt_pat.Rows.Count;
        while ( j> 0)
        {
            DataRow row_pat = dt_pat.Rows[j - 1];

            int deptid = Convert.ToInt32(row_pat[0]);
            SqlCommand cmd = new SqlCommand("select * from InjectionTable i inner Join InjectionMaster m on i.injectionid=m.injectionid inner join patientdetails p on p.patientid=i.patientid inner join EmployeeDetails e on i.docid=e.empid where (i.status='Requested' and i.deptid='" + deptid + "' ) or ( i.status='In Progress' and i.deptid='" + deptid + "' ) order by i.injecreqid", c.Con);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                GridView_InjReq.DataSource = dt;
                GridView_InjReq.DataBind();
            }
            j--;
        }
            c.Con.Close();
        
    }
    protected void GridView_InjReq_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_InjReq.PageIndex = e.NewPageIndex;
        BindGridView();
    }
    protected void GridView_InjReq_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView_InjReq.Rows[index];
            Response.Redirect("InjectionPage.aspx?InjReqNo=" + row.Cells[0].Text);
        }
    }
}