﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OPD/Nurse/NurseMaster.master" AutoEventWireup="true" CodeFile="ViewInjectionRequests.aspx.cs" Inherits="Nurse_ViewInjectionRequests" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <table>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView_InjReq" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="3" Width="800px" DataKeyNames="injecreqid" OnPageIndexChanging="GridView_InjReq_PageIndexChanging" OnRowCommand="GridView_InjReq_RowCommand" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle ForeColor="#000066" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                        <Columns>
                                  <asp:BoundField DataField="injecreqid" HeaderText="Request Id" />  
                                    <asp:BoundField DataField="injectionname" HeaderText="Particulars "/>
                                 <asp:BoundField DataField="pname" HeaderText="Patient Name" />
                                 <asp:BoundField DataField="dateofinj" HeaderText="Requested date" />
                                 <asp:BoundField DataField="empname" HeaderText="Requested By" />
                                <asp:ButtonField Text="View" CommandName="Select" ItemStyle-Width="30"  >
                                <ItemStyle Width="30px"></ItemStyle>
                        </asp:ButtonField>

                                 
                                
                               </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>
</asp:Content>

