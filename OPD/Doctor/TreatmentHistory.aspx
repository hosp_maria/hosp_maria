﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OPD/Doctor/DoctorMaster.master" AutoEventWireup="true" CodeFile="TreatmentHistory.aspx.cs" Inherits="OPD_Doctor_TreatmentHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 18px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1">
    <table class="auto-style1">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl_patno" runat="server" ForeColor="White"></asp:Label>
            </td>

        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
             <td class="auto-style2">
                               
                    <asp:GridView ID="gridview_pat" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White" runat="server" AutoGenerateColumns="False"  Width="498px" CellPadding="4" ForeColor="#333333" GridLines="None" CssClass="auto-style2" DataKeyNames="patientid" AllowPaging="True" OnPageIndexChanging="gridview_pat_PageIndexChanging" PageSize="20"  >  
                       <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>  
                        
                        <asp:BoundField DataField="patientid" HeaderText="Patient Id" />  
                        <asp:BoundField DataField="pname" HeaderText="Patient Name" />  
                        <asp:BoundField DataField="scheduledate" HeaderText="Date of Consult" />  
               
                    </Columns>  
                       <EditRowStyle BackColor="#999999" />
                       <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />

<HeaderStyle BackColor="#5D7B9D" ForeColor="White" Font-Bold="True"></HeaderStyle>
                       <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                       <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                       <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                       <SortedAscendingCellStyle BackColor="#E9E7E2" />
                       <SortedAscendingHeaderStyle BackColor="#506C8C" />
                       <SortedDescendingCellStyle BackColor="#FFFDF8" />
                       <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
                
               
             </td>
            </tr>
        <tr>
            <td class="auto-style2"></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
    </form>
</asp:Content>

