﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class OPD_Doctor_TreatmentHistory : System.Web.UI.Page
{
    conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/OPD/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            c.getCon();
            string user = Session["id"].ToString();
            SqlCommand cmd_usr = new SqlCommand("select * from UserTable where username= '" + user + "' ", c.Con);
            SqlDataAdapter sda_usr = new SqlDataAdapter(cmd_usr);
            DataTable dt_usr = new DataTable();
            sda_usr.Fill(dt_usr);
            int empid;
            int k = cmd_usr.ExecuteNonQuery();

            if (dt_usr.Rows.Count > 0)
            {
                DataRow row_usr = dt_usr.Rows[dt_usr.Rows.Count - 1];

                empid = Convert.ToInt32(row_usr[3]);
                Session["docid"] = empid;
            }
            c.Con.Close();
            BindGridView();

        }

    }
    protected void gridview_pat_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gridview_pat.PageIndex = e.NewPageIndex;
        BindGridView();
    }
    private void BindGridView()
    {
        c.getCon();
        int id = Convert.ToInt32(Session["docid"]);
        SqlCommand cmd_dept = new SqlCommand("select * from EmployeeDetails where empid= '" + Session["docid"] + "' ", c.Con);
        SqlDataAdapter sda_dept = new SqlDataAdapter(cmd_dept);
        DataTable dt_dept = new DataTable();
        sda_dept.Fill(dt_dept);

        int d = cmd_dept.ExecuteNonQuery();
        if (dt_dept.Rows.Count > 0)
        {
            DataRow row_dept = dt_dept.Rows[dt_dept.Rows.Count - 1];

            int deptid = Convert.ToInt32(row_dept[16]);
            SqlCommand cmd = new SqlCommand("select * from patientdetails p inner Join Schedule s on p.patientid=s.patientid where s.treatedby='" + Session["docid"] + "' and s.deptid='" + deptid + "' order by s.scheduledate desc", c.Con);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                gridview_pat.DataSource = dt;
                gridview_pat.DataBind();
            }
            lbl_patno.Text = "Total Patients Consulted till date- " + dt.Rows.Count;

            c.Con.Close();
        }
    }
}