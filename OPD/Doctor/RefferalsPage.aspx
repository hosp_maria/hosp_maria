﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OPD/Doctor/DoctorMaster.master" AutoEventWireup="true" CodeFile="RefferalsPage.aspx.cs" Inherits="OPD_Doctor_RefferalsPage" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    
    <style type="text/css">
        .auto-style1 {
            height: 20px;
        }
    </style>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
    <table class="auto-style1">
         <tr>
            <td colspan="2"><div class="drop">
<ul class="drop_menu">
    <li><asp:LinkButton ID="link_refdept" runat="server" OnClick="link_refdept_Click">Referrals from other Departments</asp:LinkButton>
    </li>
      <li><asp:LinkButton ID="link_refcas" runat="server" OnClick="link_refcas_Click">Referrals from Casuality</asp:LinkButton>
    </li>
  </ul></div></td>
                                       </tr>
        <tr>
            <td>

                <asp:MultiView ID="mulview_ref" runat="server">
                    <table class="auto-style1">
                        <tr>
                            <td>
                                <asp:View ID="view_dept" runat="server">
                                    <table class="auto-style1">
                                        <tr>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td class="auto-style1"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_dept" runat="server" Font-Size="Large" ForeColor="#FF99FF" Text="No Refferals from Other Departments"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="refid" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" Width="800px">
                                                    <Columns>
                                                        <asp:BoundField DataField="patientid" HeaderText="Patient Id">
                                                        <ItemStyle Width="5%" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="pname" HeaderText="Patient Name">
                                                        <ItemStyle Width="15%" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="empname" HeaderText="Refferenced By">
                                                        <ItemStyle Width="20%" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="refdate" HeaderText="Refferenced Date">
                                                        <ItemStyle Width="15%" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="remarks" HeaderText="Remarks">
                                                        <ItemStyle Width="35%" />
                                                        </asp:BoundField>
                                                        <asp:ButtonField CommandName="Select" Text="View">
                                                        <ItemStyle Width="30px" />
                                                        </asp:ButtonField>
                                                    </Columns>
                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </asp:View>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:View ID="view_cas" runat="server">
                                    <table class="auto-style1">
                                        <tr>
                                            <td>
                                                <asp:Label ID="lbl_cas" runat="server" Font-Size="Large" ForeColor="#FF99FF" Text="No Refferals from Casuality"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:GridView ID="GridView2" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="refid" OnPageIndexChanging="GridView2_PageIndexChanging" OnRowCommand="GridView2_RowCommand" Width="800px">
                                                    <Columns>
                                                        <asp:BoundField DataField="patientid" HeaderText="Patient Id">
                                                        <ItemStyle Width="5%" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="pname" HeaderText="Patient Name">
                                                        <ItemStyle Width="15%" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="empname" HeaderText="Refferenced By">
                                                        <ItemStyle Width="20%" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="refdate" HeaderText="Refferenced Date">
                                                        <ItemStyle Width="15%" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="remarks" HeaderText="Remarks">
                                                        <ItemStyle Width="35%" />
                                                        </asp:BoundField>
                                                        <asp:ButtonField CommandName="Select" Text="View">
                                                        <ItemStyle Width="30px" />
                                                        </asp:ButtonField>
                                                    </Columns>
                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </asp:View>
                            </td>
                        </tr>
                    </table>
                </asp:MultiView>

            </td>
        </tr>
        </table>
        </form>
</asp:Content>

