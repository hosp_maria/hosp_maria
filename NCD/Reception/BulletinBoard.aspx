﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NCD/Reception/NCDRecMaster.master" AutoEventWireup="true" CodeFile="BulletinBoard.aspx.cs" Inherits="NCD_Reception_BulletinBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <table class="auto-style1">
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lbl_msg" runat="server" Font-Size="Large" ForeColor="#FF9999" Text="NO MESSAGES FOUND"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:LinkButton ID="link_delmes" runat="server" OnClick="link_delmes_Click">Delete Messages</asp:LinkButton>
                    <asp:LinkButton ID="link_del" runat="server" OnClientClick="javascript:return Confirmationbox();" onclick="btnDelete_Click">Delete Selected records</asp:LinkButton>
                    <asp:LinkButton ID="link_can" runat="server" OnClick="link_can_Click">Cancel</asp:LinkButton>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" Width="1000px" DataKeyNames="receiptid" OnRowDataBound="GridView1_RowDataBound" AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3">
                        <Columns>
                            <asp:TemplateField ItemStyle-Width="50">
<ItemTemplate>
<asp:CheckBox ID="chkSelect" runat="server" />
</ItemTemplate>

<ItemStyle Width="50px"></ItemStyle>
</asp:TemplateField>
                            <asp:TemplateField HeaderText = "#" ItemStyle-Width="100">
        <ItemTemplate>
            <asp:Label ID="lblRowNumber" Text='<%# Container.DataItemIndex + 1 %>' runat="server" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"/>
        </ItemTemplate>

<ItemStyle Width="100px"></ItemStyle>
    </asp:TemplateField>
        <asp:HyperLinkField DataTextField="subject" DataNavigateUrlFields="receiptid" DataNavigateUrlFormatString="MessagePage.aspx?Id={0}"
            HeaderText="Subject" ItemStyle-Width = "150"  HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="150px"></ItemStyle>
                            </asp:HyperLinkField>
        <asp:BoundField DataField="date" HeaderText="Date" ItemStyle-Width = "150" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" >
<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="150px"></ItemStyle>
                            </asp:BoundField>
           <asp:BoundField DataField="status" HeaderText="" ItemStyle-Width = "150" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" >

<HeaderStyle HorizontalAlign="Center"></HeaderStyle>

<ItemStyle HorizontalAlign="Center" Width="150px"></ItemStyle>
                            </asp:BoundField>

                             </Columns>
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle ForeColor="#000066" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>
</asp:Content>



