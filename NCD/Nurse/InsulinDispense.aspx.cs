﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class NCD_Nurse_InsulinDispense : System.Web.UI.Page
{
    conclass c = new conclass();
    int drugno = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            bindlabels();
        
            if (Session["Id"] == null)
                Response.Redirect("~/NCD/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
        }

    }
   
   
    protected void btn_disp_Click(object sender, EventArgs e)
    {
        c.getCon();

        drugno = Convert.ToInt32(Request.QueryString["PresId"].ToString());

        SqlCommand cmd = new SqlCommand("select m.medid,s.stockvalue,p.patientid from Prescription p inner join MedMaster m on p.medid=m.MedId inner join NCDInsulinStock s on s.medid=m.Medid where presid='" + drugno + "'", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            string medid = dt.Rows[0][0].ToString();
            string stock = dt.Rows[0][1].ToString();
            string pno = dt.Rows[0][2].ToString();
            int updstock = Convert.ToInt32(stock) - 1;
            SqlCommand cmds = new SqlCommand("select batchid from InsulinBatch where expdate='" + txt_exp.Text + "'", c.Con);
            DataTable dts = new DataTable();
            SqlDataAdapter das = new SqlDataAdapter(cmds);
            das.Fill(dts);
            if (dts.Rows.Count > 0)
            {
                string batchid = dts.Rows[0][0].ToString();
                String s = "update NCDInsulinStock set stockvalue='" + updstock + "' where medid='" + medid + "' and batchid='" + batchid + "'";
                SqlCommand cmdsd = new SqlCommand(s, c.Con);
                cmdsd.ExecuteNonQuery();
                String str = "update Prescription set status='Dispensed' where presid='" + drugno + "'";
                SqlCommand cmd_stat = new SqlCommand(str, c.Con);
                cmd_stat.ExecuteNonQuery();
                Response.Redirect("Homepagenurse.aspx");

                c.Con.Close();
            }
        }
        else
            Response.Write("<script>alert('No Medicine batch found with that expiry date');</script>");
        
    }
    public void bindlabels()
    {
        drugno = Convert.ToInt32(Request.QueryString["PresId"].ToString());
        c.getCon();
        SqlCommand cmd = new SqlCommand("select pat.pname,m.medname,p.potency,p.dosage,p.frequency,p.no_of_days,p.remarks,p.dateofpres,s.stockvalue from Prescription p inner join medMaster m on p.medid=m.MedId inner join patientdetails pat on pat.patientid=p.patientid inner join NCDInsulinStock s on s.medid=m.Medid where presid='" + drugno + "'", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            lbl_patname.Text = dt.Rows[0][0].ToString();
            lbl_drug.Text = dt.Rows[0][1].ToString();
            lbl_dos.Text = dt.Rows[0][3].ToString();
            lbl_freq.Text = dt.Rows[0][4].ToString();
            lbl_instr.Text = dt.Rows[0][6].ToString();
            lbl_noofdays.Text = dt.Rows[0][5].ToString();
            lbl_pres.Text = dt.Rows[0][7].ToString();
            lbl_avial.Text = dt.Rows[0][8].ToString();
        }
        c.Con.Close();
    }
    
}