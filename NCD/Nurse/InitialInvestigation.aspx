﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NCD/Nurse/NCDNurseMaster.master" AutoEventWireup="true" CodeFile="InitialInvestigation.aspx.cs" Inherits="NCD_Nurse_InitialInvestigation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
    <table class="nav-justified">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        
                                        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Enter Cardno</td>
            <td style="margin-left: 40px">
                <asp:TextBox ID="txt_cardno" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:Button ID="btn_search" runat="server" Text="Search" OnClick="btn_search_Click" CssClass="btn" />
            </td>
        </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Name</td>
            <td>
                <asp:Label ID="lbl_name" runat="server"></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Gender</td>
            <td>
                <asp:Label ID="lbl_gender" runat="server"></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Age</td>
            <td>
                <asp:Label ID="lbl_dob" runat="server"></asp:Label>
            </td>
            <td></td>
        </tr>
                                        <tr><td>&nbsp;</td></tr>

                                        <tr><td>&nbsp;</td></tr>
        <tr><td> <div class="drop">
<ul class="drop_menu">
<li><asp:LinkButton ID="link_vitals" runat="server" OnClick="link_vitals_Click">Check Vitals</asp:LinkButton></li>
 <li>   <asp:LinkButton ID="link_rbs" runat="server" OnClick="link_rbs_Click">Check RBS</asp:LinkButton></li>
  </ul>
</div>
</td></tr>                                        <tr><td colspan="3">&nbsp;</td></tr>
        <tr>
            <td colspan="3">
                <asp:MultiView ID="MultiView1" runat="server">
                    <table class="nav-justified">
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:View ID="View2" runat="server">
                                        <table >
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr><tr>
            <td>Particulars</td>
            <td>
                <asp:DropDownList ID="ddl_vital" runat="server">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
        </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Value</td>
            <td>
                <asp:TextBox ID="txt_value" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Remarks</td>
            <td>
                <asp:TextBox ID="txt_remarks" runat="server" Height="45px" TextMode="MultiLine" Width="264px"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btn_add" runat="server" OnClick="btn_add_Click" Text="Add" CssClass="btn" />
            </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td  colspan="3">
                <asp:GridView ID="GridView_Vitals" AutoGenerateColumns="False" runat="server" CellPadding="3" AllowPaging="True" OnPageIndexChanging="GridView_Vitals_PageIndexChanging" Width="1300px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                     <Columns><asp:BoundField DataField="vital" HeaderText="Particulars "/>
                                 <asp:BoundField DataField="value" HeaderText="Value" />
                                 <asp:BoundField DataField="remarks" HeaderText="Remarks" />
                                
                               </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;</td>
        </tr>
    </table>
                                </asp:View>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:View ID="View3" runat="server">
                                    <table>
                                         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                &nbsp;</td>
        </tr>
                                         <tr>
                                             <td>Value</td>
                                             <td>
                                                 <asp:TextBox ID="txt_valuerbs" runat="server"></asp:TextBox>
                                             </td>
                                         </tr>
         <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>Remarks</td>
            <td>
                <asp:TextBox ID="txt_remrbs" runat="server" Height="45px" TextMode="MultiLine" Width="264px"></asp:TextBox>
            </td>
        </tr>
        <tr>
            
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btn_addrbs" runat="server" OnClick="btn_addrbs_Click" Text="Add" CssClass="btn" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td  colspan="2">
                <asp:GridView ID="GridView_RBS" AutoGenerateColumns="False" runat="server" CellPadding="3" AllowPaging="True" OnPageIndexChanging="GridView_RBS_PageIndexChanging" Width="800px" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px">
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                     <Columns>
                                 <asp:BoundField DataField="value" HeaderText="Value" />
                                 <asp:BoundField DataField="remarks" HeaderText="Remarks" />
                                 <asp:BoundField DataField="date" HeaderText="Date" />
                                 <asp:BoundField DataField="empname" HeaderText="Taken By" />

                               </Columns>
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;</td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;</td>
        </tr>
    </table>
                                    </table>
                                </asp:View>
                            </td>
                        </tr>
                    </table>
                </asp:MultiView>
            </td>
        </tr>
        </table>
        </form>
</asp:Content>

