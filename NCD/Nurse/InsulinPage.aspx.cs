﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class NCD_Nurse_InsulinPage : System.Web.UI.Page
{
    conclass c = new conclass();
    int deptid = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/NCD/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }

            c.getCon();
            lbl_ins.Visible = false;

            SqlCommand cmd_dept = new SqlCommand("select deptid from Department d inner join EmpTypeMaster e on e.emptypeid=d.emptype where e.emptypename like 'D%' and d.deptname like 'NCD%'", c.Con);
            SqlDataAdapter sda_dept = new SqlDataAdapter(cmd_dept);
            DataTable dt_dept = new DataTable();
            sda_dept.Fill(dt_dept);

            int d = cmd_dept.ExecuteNonQuery();

            if (dt_dept.Rows.Count > 0)
            {
                DataRow row_dept = dt_dept.Rows[dt_dept.Rows.Count - 1];

                deptid = Convert.ToInt32(row_dept[0]);
                BindGridView();

            }
        }
    }
    protected void gridview_pat_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("InsulinDispense.aspx?PresId=" + gridview_pat.DataKeys[index].Value.ToString());
       
        }
    }
    public void BindGridView()
    {
        SqlCommand cmd_med = new SqlCommand("select MedId from MedMaster where MedName like '%Insulin%' or MedName like '%insulin%'", c.Con);
        SqlDataAdapter sda_med = new SqlDataAdapter(cmd_med);
        DataTable dt_med = new DataTable();
        sda_med.Fill(dt_med);

        int d = cmd_med.ExecuteNonQuery();

        if (dt_med.Rows.Count > 0)
        {
            DataRow row_med = dt_med.Rows[dt_med.Rows.Count - 1];

            int medid = Convert.ToInt32(row_med[0]);
        
         SqlCommand cmd = new SqlCommand("select * from Prescription p inner Join patientdetails d on p.patientid=d.patientid where p.deptid=@deptid and p.medid=@medid and dateofpres=@date and p.status=@status order by p.presid", c.Con);
        cmd.Parameters.AddWithValue("@date", DateTime.Today);
        cmd.Parameters.AddWithValue("@deptid", deptid);
        cmd.Parameters.AddWithValue("@status", "Prescribed");
        cmd.Parameters.AddWithValue("@medid", medid);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            gridview_pat.DataSource = dt;
            gridview_pat.DataBind();
        }
        else
            lbl_ins.Visible = true;

    }
    }
}