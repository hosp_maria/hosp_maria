﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class NCD_Nurse_TallyOrder : System.Web.UI.Page
{
    conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
      if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/NCD/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
        
            BindLabels();
        }
    }
    public void BindLabels()
    {
        c.getCon();
        int orderno = Convert.ToInt32(Request.QueryString["OrderNo"].ToString());
        lbl_orderid.Text = orderno.ToString();
        SqlCommand cmd = new SqlCommand("select m.medname,o.orderamt,o.dateoforder from NCDInsulinStock s inner join medMaster m on s.medid=m.MedId inner join NCDInsulinOrder o on o.medid=s.medid where o.orderid=@ono and status=@stat order by orderid", c.Con);
        cmd.Parameters.AddWithValue("@ono", orderno);
        cmd.Parameters.AddWithValue("@stat", "Pending");
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            lbl_medname.Text = dt.Rows[0][0].ToString();
            lbl_orderamt.Text = dt.Rows[0][1].ToString();
            lbl_orderdate.Text = dt.Rows[0][2].ToString();
        }
        c.Con.Close();
    }
    protected void btn_tally_Click(object sender, EventArgs e)
    {
        c.getCon();
        int orderno = Convert.ToInt32(Request.QueryString["OrderNo"].ToString());

        SqlCommand cmd = new SqlCommand("select m.stockvalue from NCDInsulinOrder i inner join NCDInsulinStock m on i.medid=m.medid where i.orderid='" + orderno + "'", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            DataRow rows = dt.Rows[dt.Rows.Count - 1];
            int stk = Convert.ToInt32(rows[0]);

            string upd = "update NCDInsulinOrder set status='Fullfilled',dateoffulfil='" + DateTime.Today + "' where orderid='" + orderno + "'";
            SqlCommand cmds = new SqlCommand(upd, c.Con);
            cmds.ExecuteNonQuery();

            int stock = Convert.ToInt32(txt_amtrec.Text) + Convert.ToInt32(stk);
            string update = "update s set s.stockvalue='" + stock + "' from  NCDInsulinStock s inner join NCDInsulinOrder o on o.medid=s.medid   where o.orderid='" + orderno + "'";
            SqlCommand cmdup = new SqlCommand(update, c.Con);
            cmdup.ExecuteNonQuery();
            c.Con.Close();
            Response.Redirect("UpdateStock.aspx");
        }
    }
}



