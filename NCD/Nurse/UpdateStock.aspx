﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NCD/Nurse/NCDNurseMaster.master" AutoEventWireup="true" CodeFile="UpdateStock.aspx.cs" Inherits="NCD_Nurse_UpdateStock" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 18px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <table class="auto-style1">
            <tr>
                <td>
                    <asp:Label ID="lbl_med" runat="server" Font-Size="Large" ForeColor="White"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Font-Size="Large" ForeColor="#FF0066" Text="No Pending records"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">
                <asp:GridView ID="GridView1" runat="server" BackColor="White" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Horizontal" Width="1000px" AllowPaging="True" AutoGenerateColumns="False" PageSize="50" DataKeyNames="orderid" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand1" style="margin-right: 43px" >
                    <AlternatingRowStyle BackColor="#F7F7F7" />
                    <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                    <HeaderStyle BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" />
                    <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                    <RowStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" />
                    <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                    <SortedAscendingCellStyle BackColor="#F4F4FD" />
                    <SortedAscendingHeaderStyle BackColor="#5A4C9D" />
                    <SortedDescendingCellStyle BackColor="#D8D8F0" />
                    <SortedDescendingHeaderStyle BackColor="#3E3277" />
                     <Columns>  

                        <asp:BoundField DataField="medname" ItemStyle-Width="25%" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderText="Medicine Name" />  
                        <asp:BoundField DataField="orderamt" ItemStyle-Width="25%" HeaderText="Order Amount" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />  
                        <asp:BoundField DataField="dateoforder" ItemStyle-Width="25%" HeaderText="Date Of Order" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"/>  
                    
                      <asp:ButtonField Text="View" ItemStyle-Width="25%" CommandName="Select"   >

                        
                             
<ItemStyle Width="30px"></ItemStyle>
                        </asp:ButtonField>

                             
                    </Columns>  

                </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>
</asp:Content>

