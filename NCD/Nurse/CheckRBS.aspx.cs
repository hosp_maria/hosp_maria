﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class NCD_Nurse_CheckRBS : System.Web.UI.Page
{
    conclass c = new conclass();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/NCD/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
        }
   }
    protected void btn_search_Click(object sender, EventArgs e)
    {
        c.getCon();
        int pno = Convert.ToInt32(txt_cardno.Text);
        SqlCommand cmd_pat = new SqlCommand("select * from patientdetails where patientid= '" + pno + "' ", c.Con);
        SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
        DataTable dt_pat = new DataTable();
        sda_pat.Fill(dt_pat);


        int k_pat = cmd_pat.ExecuteNonQuery();

        if (dt_pat.Rows.Count > 0)
        {
            DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];

            String name = Convert.ToString(row_pat[1]);
            String pid = Convert.ToString(row_pat[0]);

            DateTime dob = Convert.ToDateTime(row_pat[7]);
            String gender = Convert.ToString(row_pat[14]);
            int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;
            lbl_dob.Text = Convert.ToString(age) + " years";
            lbl_gender.Text = gender;
            lbl_name.Text = name;
        }
        c.Con.Close();
        BindGridView_RBS();

    }

    protected void btn_add_Click(object sender, EventArgs e)
    {
        c.getCon();
        int pno = Convert.ToInt32(txt_cardno.Text);

        String s = "insert into NCDRBSTable values('" + txt_value.Text + "','" + txt_remarks.Text + "','" + DateTime.Now + "','" + txt_cardno.Text + "','" + Session["uid"] + "')";
        SqlCommand cmds = new SqlCommand(s, c.Con);
        cmds.ExecuteNonQuery();

        c.Con.Close();
        txt_remarks.Text = " ";
        txt_value.Text = " ";
        BindGridView_RBS();
    }
    protected void GridView_RBS_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView_RBS.PageIndex = e.NewPageIndex;
        BindGridView_RBS();
    }
    public void BindGridView_RBS()
    {
        c.getCon();
        int pno = Convert.ToInt32(txt_cardno.Text);

        SqlCommand cmd = new SqlCommand("select * from NCDRBSTable n inner join NCDEmployeeDetails e on e.empid=n.nurseid where patientid=@pno order by rbsid desc", c.Con);
        cmd.Parameters.AddWithValue("@pno", pno);

        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            GridView_RBS.DataSource = dt;
            GridView_RBS.DataBind();
        }
        c.Con.Close();

    }
}