﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NCD/Nurse/NCDNurseMaster.master" AutoEventWireup="true" CodeFile="InsulinDispense.aspx.cs" Inherits="NCD_Nurse_InsulinDispense" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 18px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <table class="auto-style1">
            <tr>
                <td class="auto-style2"></td>
                <td class="auto-style2"></td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Patient Name</td>
                <td>
                    <asp:Label ID="lbl_patname" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Drug Name</td>
                <td>
                    <asp:Label ID="lbl_drug" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Dosage</td>
                <td>
                    <asp:Label ID="lbl_dos" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; No of Days</td>
                <td>
                    <asp:Label ID="lbl_noofdays" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Frequency</td>
                <td>
                    <asp:Label ID="lbl_freq" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Instructions</td>
                <td>
                    <asp:Label ID="lbl_instr" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Available Number in Stock</td>
                <td>
                    <asp:Label ID="lbl_avial" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Prescribed Date</td>
                <td>
                    <asp:Label ID="lbl_pres" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Expiry Date of Insulin Given&nbsp;</td>
                <td class="auto-style2"> <asp:textbox id="txt_exp" runat="server"></asp:textbox><cc1:CalendarExtender ID="CalendarExtender1" TargetControlID="txt_exp" runat="server"></cc1:CalendarExtender>
        <asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server"
		controltovalidate="txt_exp" errormessage="Please Enter Expiry Date"
		setfocusonerror="true" display="None"></asp:requiredfieldvalidator>
        <cc1:validatorcalloutextender id=
		"RequiredFieldValidator1_ValidatorCalloutExtender"
		runat="server" targetcontrolid="RequiredFieldValidator1"
		behaviorid="textValidator" enabled="True">
	</cc1:validatorcalloutextender>
   </td>
            </tr>
            <tr>
                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                </td>
                <td>
                    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
                    </cc1:ToolkitScriptManager>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Button ID="btn_disp" runat="server" OnClick="btn_disp_Click" Text="Dispense" CssClass="btn" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
           
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
           
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
           
        </table>
    </form>
</asp:Content>

