﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.DataVisualization.Charting;
using System.Data.SqlClient;
public partial class NCD_Nurse_ViewStock : System.Web.UI.Page
{
    conclass c = new conclass();
    int injid = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/NCD/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }

            BindLabels();
            chart_inj.Visible = false;
        }
    }
    public void BindChart()
    {
        c.getCon();
        SqlCommand cmd_chart = new SqlCommand("select orderamt,dateoforder  from NCDInsulinOrder where medid='" + lbl_id.Text + "' and status='Fullfilled'", c.Con);
        SqlDataAdapter sda_chart = new SqlDataAdapter(cmd_chart);
        DataTable dt_chart = new DataTable();
        sda_chart.Fill(dt_chart);
        int k_pat = cmd_chart.ExecuteNonQuery();
        int amt = 0;
        if (dt_chart.Rows.Count > 0)
        {
            int[] x = new int[dt_chart.Rows.Count];
            int[] y = new int[dt_chart.Rows.Count];
            for (int i = 0; i < dt_chart.Rows.Count; i++)
            {

                int order = Convert.ToInt32(dt_chart.Rows[i][0]);
                DateTime date = Convert.ToDateTime(dt_chart.Rows[i][1]);
                int year = date.Year;
                amt = amt + order;
                x[i] = year;
                y[i] = order;
            }
            amt = amt / dt_chart.Rows.Count;
            chart_inj.Series[0].Points.DataBindXY(x, y);
            lbl_est.Text = amt.ToString();


        }


    }
    public void BindLabels()
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("select MedId from MedMaster where MedName like '%Insulin%' or MedName like '%insulin%' ", c.Con);
        SqlDataAdapter sda= new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        sda.Fill(dt);
        int k = cmd.ExecuteNonQuery();
        if (dt.Rows.Count > 0)
        {
            DataRow row = dt.Rows[dt.Rows.Count - 1];
            String drugid = Convert.ToString(row[0]);
            SqlCommand cmd_pat = new SqlCommand("select i.MedId,i.MedName,s.stockvalue  from MedMaster i inner join NCDInsulinStock s on i.MedId=s.medid where s.medid= '" + drugid + "' ", c.Con);
            SqlDataAdapter sda_pat = new SqlDataAdapter(cmd_pat);
            DataTable dt_pat = new DataTable();
            sda_pat.Fill(dt_pat);
            int k_pat = cmd_pat.ExecuteNonQuery();
            if (dt_pat.Rows.Count > 0)
            {
                DataRow row_pat = dt_pat.Rows[dt_pat.Rows.Count - 1];
                String iname = Convert.ToString(row_pat[1]);
                int stock = Convert.ToInt32(row_pat[2]);
                int id = Convert.ToInt32(row_pat[0]);
                lbl_id.Text = id.ToString();
                lbl_name.Text = iname;
                lbl_currstock.Text = stock.ToString();


            }
            c.Con.Close();
        }
    }
    protected void btn_add_Click(object sender, EventArgs e)
    {
        c.getCon();
        String s_in = "insert into NCDInsulinOrder values('" + lbl_id.Text + "','" + txt_amount.Text + "','" + DateTime.Today + "','Pending',' ')";
        SqlCommand cmdsin = new SqlCommand(s_in, c.Con);

        cmdsin.ExecuteNonQuery();

        c.Con.Close();
        Response.Redirect("Homepagenurse.aspx");
    }

    protected void link_previous_Click(object sender, EventArgs e)
    {
        chart_inj.Visible = true;

        BindChart();
    }
}