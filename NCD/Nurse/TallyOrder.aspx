﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NCD/Nurse/NCDNurseMaster.master" AutoEventWireup="true" CodeFile="TallyOrder.aspx.cs" Inherits="NCD_Nurse_TallyOrder" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <table class="auto-style1">
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Order Id</td>
                <td>
                    <asp:Label ID="lbl_orderid" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Drug Name</td>
                <td>
                    <asp:Label ID="lbl_medname" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Order Amount</td>
                <td>
                    <asp:Label ID="lbl_orderamt" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Order Date</td>
                <td>
                    <asp:Label ID="lbl_orderdate" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>Amount Received</td>
                <td>
                    <asp:TextBox ID="txt_amtrec" runat="server" CssClass="twitter"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btn_tally" runat="server" OnClick="btn_tally_Click" Text="Tally Order" CssClass="btn" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </form>
</asp:Content>

