﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
public partial class NCD_Doctor_RefferalsPage : System.Web.UI.Page
{
    conclass c = new conclass();
    int deptid = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/NCD/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
            mulview_ref.ActiveViewIndex = 0;
            lbl_cas.Visible = false;
            lbl_dept.Visible = false;
            c.getCon();
            string user = Session["id"].ToString();
            BindGridView1();
            BindGridView2();

            c.Con.Close();
        }
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("RefferalConsult.aspx?RefNo=" + GridView1.DataKeys[index].Value.ToString());
        }
    }
    protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        if (e.CommandName == "Select")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("RefferalsCasuality.aspx?RefNo=" + GridView2.DataKeys[index].Value.ToString());
        }
    }
    private void BindGridView1()
    {
        SqlCommand cmddept = new SqlCommand("select deptid from Department d inner join EmpTypeMaster e on e.emptypeid=d.emptype where e.emptypename like 'D%' and d.deptname like 'NCD%'", c.Con);
        SqlDataAdapter sdadept = new SqlDataAdapter(cmddept);
        DataTable dtdept = new DataTable();
        sdadept.Fill(dtdept);

        int idept = cmddept.ExecuteNonQuery();
        int j = dtdept.Rows.Count;
        if (j > 0)
        {
            DataRow row = dtdept.Rows[dtdept.Rows.Count - 1];

            int did = Convert.ToInt32(row[0]);
            int eid = 0;
            SqlCommand cmd = new SqlCommand("select * from patientdetails p inner Join ReferralTableOP r on p.patientid=r.patientid inner join EmployeeDetails c on c.empid=r.refby where (r.reftodept=@dptno and r.status=@status and r.reftodoc=@empid) or  (r.reftodept=@dptno and r.status=@status and r.reftodoc=@eid) order by r.refdate", c.Con);
            cmd.Parameters.AddWithValue("@date", DateTime.Today);
            cmd.Parameters.AddWithValue("@dptno", did);
            cmd.Parameters.AddWithValue("@status", "Referred");
            cmd.Parameters.AddWithValue("@empid", Session["docid"]);
            cmd.Parameters.AddWithValue("@eid", eid);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                GridView1.DataSource = dt;
                GridView1.DataBind();
            }
            else
                lbl_dept.Visible = true;
        }
    }
    private void BindGridView2()
    {
        SqlCommand cmddept = new SqlCommand("select deptid from Department d inner join EmpTypeMaster e on e.emptypeid=d.emptype where e.emptypename like 'D%' and d.deptname like 'NCD%'", c.Con);
                SqlDataAdapter sdadept = new SqlDataAdapter(cmddept);
                DataTable dtdept = new DataTable();
                sdadept.Fill(dtdept);

                int idept = cmddept.ExecuteNonQuery();
                int j = dtdept.Rows.Count;
                if (j > 0)
                {
                    DataRow row = dtdept.Rows[dtdept.Rows.Count - 1];

                    int did = Convert.ToInt32(row[0]);
                    int eid = 0;
                    SqlCommand cmd = new SqlCommand("select * from patientdetails p inner Join ReferralTableCasuality r on p.patientid=r.patientid inner join CasualityEmployeeDetails c on c.empid=r.refby where (r.reftodept=@dptno and r.status=@status and r.reftodoc=@empid) or  (r.reftodept=@dptno and r.status=@status and r.reftodoc=@eid) order by r.refdate", c.Con);
                    int id = Convert.ToInt32(Session["docid"]);
                    cmd.Parameters.AddWithValue("@date", DateTime.Today);
                    cmd.Parameters.AddWithValue("@dptno", did);
                    cmd.Parameters.AddWithValue("@status", "Referred");
                    cmd.Parameters.AddWithValue("@empid", id);
                    cmd.Parameters.AddWithValue("@eid", eid);
                    DataTable dt = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);


                    if (dt.Rows.Count > 0)
                    {
                        GridView2.DataSource = dt;
                        GridView2.DataBind();
                    }
                    else lbl_cas.Visible = true;
                }
    }
    protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView2.PageIndex = e.NewPageIndex;
        BindGridView2();
    }
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindGridView1();
    }
    protected void link_refdept_Click(object sender, EventArgs e)
    {
        mulview_ref.ActiveViewIndex = 0;
    }
    protected void link_refcas_Click(object sender, EventArgs e)
    {
        mulview_ref.ActiveViewIndex = 1;

    }
}