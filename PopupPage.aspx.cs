﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PopupPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string msg = "Session Expired";
            ClientScript.RegisterStartupScript(this.GetType(), msg, "<script type='text/javascript'>alert('" + msg + "');window.location='SystemHome.aspx';</script>'");

        }

    }
}