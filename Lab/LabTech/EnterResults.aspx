﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Lab/LabTech/LabTechMaster.master" AutoEventWireup="true" CodeFile="EnterResults.aspx.cs" Inherits="Lab_LabTech_EnterResults" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<script runat="server">

</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .auto-style1 {
            height: 20px;
        }
    </style>
   <%-- <script language="javascript" type="text/javascript">
        function CallPrint(strid) {
            var prtContent = document.getElementById(strid);
            var WinPrint = window.open('', '', 'letf=0,top=0,width=1,height=1,toolbar=0,scrollbars=0,status=0');
            WinPrint.document.write(prtContent.innerHTML);
            WinPrint.document.close();
            WinPrint.focus();
            WinPrint.print();
            WinPrint.close();
            prtContent.innerHTML = strOldOne;
        }
</script>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <form id="form1" runat="server">
        <center>
        <table>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
             <tr>
            <td> <ajax:ToolkitScriptManager ID="toolkit1" runat="server"></ajax:ToolkitScriptManager></td>
            <td>&nbsp;</td>
        </tr>
            <tr>
                <td>Enter Patient Card No</td>
                <td>
                    <asp:TextBox ID="txt_id" runat="server" CssClass="twitter" ></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>Select the department</td>
                <td>
                    <asp:DropDownList ID="ddl_dept" runat="server">
                        <asp:ListItem>[Select]</asp:ListItem>
                        <asp:ListItem>OP</asp:ListItem>
                        <asp:ListItem>NCD</asp:ListItem>
                        <asp:ListItem>Casuality</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>Date</td>
                <td> <asp:TextBox ID="txtDate" runat="server" CssClass="twitter" />
<ajax:CalendarExtender ID="CalendarExtender1" TargetControlID="txtDate" Format="MM/dd/yyyy" runat="server">
</ajax:CalendarExtender></td>
            </tr>
            <tr>
                <td class="auto-style1"></td>
                <td class="auto-style1"></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:Button ID="btn_search" runat="server" CssClass="btn" Text="Search" OnClick="btn_search_Click" />
                </td>
            </tr>
            <tr>
                <td class="auto-style1"></td>
                <td class="auto-style1"></td>
            </tr>
             </table>
          <div id="bill">
                <table id="tab_print" runat="server" style="font-size: medium; width: 640px;"> 
                  <tr>
               <td>
                   &nbsp;</td>
               <td>
                   &nbsp;</td>
           </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView ID="gridview_pat" HeaderStyle-BackColor="#3AC0F2"  OnRowCommand="gridview_pat_RowCommand" HeaderStyle-ForeColor="White" runat="server" AutoGenerateColumns="False" Width="498px" CellPadding="3" CssClass="auto-style2" DataKeyNames="labresid" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"  >  
                    <Columns>  
                        <asp:BoundField DataField="testname" HeaderText="Test Name" />  
                         
                          <asp:ButtonField Text="View" CommandName="Select" ItemStyle-Width="30"  >

                        
                             
<ItemStyle Width="30px"></ItemStyle>
                        </asp:ButtonField>
                    </Columns>  
                       <FooterStyle BackColor="White" ForeColor="#000066" />


<HeaderStyle BackColor="#006699" ForeColor="White" Font-Bold="True"></HeaderStyle>
                       <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                       <RowStyle ForeColor="#000066" />
                       <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                       <SortedAscendingCellStyle BackColor="#F1F1F1" />
                       <SortedAscendingHeaderStyle BackColor="#007DBB" />
                       <SortedDescendingCellStyle BackColor="#CAC9C9" />
                       <SortedDescendingHeaderStyle BackColor="#00547E" />
                </asp:GridView></td>
            </tr>
            
            <tr>
                <td colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
            </tr>
            
            <tr>
                <td colspan="2">
                    <asp:Label ID="lbl_cas" runat="server" ForeColor="Red" Text="No Pending Request"></asp:Label>
                </td>
            </tr></table>
                    </div>
        <table>
            <tr>
                <td>
                    &nbsp;</td>
                <td>&nbsp;</td>
            </tr>
     
                </table>
                
        <p>&nbsp;</p>
    <div>
    <br /><br />
    </div></center>
    </form>
</asp:Content>


