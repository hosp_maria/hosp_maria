﻿//enter result of tests from op department
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
public partial class Lab_LabTech_EnterResultPat : System.Web.UI.Page
{
    conclass c = new conclass();
    protected void Page_Load(object sender, EventArgs e)
    {

      if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Lab/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
        
            lbl_cas.Visible = false;
            int patno = Convert.ToInt32(Request.QueryString["PatId"].ToString());
            string date = Convert.ToString(Request.QueryString["Date"].ToString());
            lbl_date.Text = date;
            lbl_pat.Text = patno.ToString();
            bind_grid_ncd();
        }
    }
    public void bind_grid_ncd()
    {

        c.getCon();
        SqlCommand cmd = new SqlCommand("SELECT m.testmasterid,m.testname,n.labresid from LabResult n inner join LabSubTestMaster m on m.testmasterid=n.labmasterid inner join LabRequest r on r.labreqid=n.labreqid  inner join patientdetails p on p.patientid=r.patientid where n.dateofres=' ' and r.patientid=@pno and r.dateofreq=@date and r.status=@status", c.Con);

        // SqlCommand cmd = new SqlCommand("SELECT * from NCDLabRequest n inner join LabTestMaster m on m.testmasterid=n.testmasterid inner join patientdetails p on p.patientid=n.patientid inner join NCDEmployeeDetails e on e.empid=n.docid where n.patientid=@pno and dateofreq=@date and n.status=@status", c.Con);
        cmd.Parameters.AddWithValue("@pno", lbl_pat.Text);
        cmd.Parameters.AddWithValue("@date", lbl_date.Text);
        cmd.Parameters.AddWithValue("@status", "Paid");

        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {

            gridview_pat.DataSource = dt;
            gridview_pat.DataBind();

        }
        else
            lbl_cas.Visible = true;

    }
    protected void gridview_pat_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            int index = Convert.ToInt32(e.CommandArgument);
            Response.Redirect("ResEnter.aspx?ReqId=" + gridview_pat.DataKeys[index].Value.ToString());

        }

    }

}