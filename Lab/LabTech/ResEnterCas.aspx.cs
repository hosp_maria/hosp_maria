﻿//enter value of each test result casuality
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Data.SqlClient;
public partial class Lab_LabTech_ResEnterCas : System.Web.UI.Page
{
    conclass c = new conclass();
    int reqid = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Lab/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }

            c.getCon();
            lbl_pat.Visible = false;
            lbl_date.Visible = false;
            ViewState["reqid"] = Convert.ToInt32(Request.QueryString["ReqId"].ToString());
            SqlCommand cmd = new SqlCommand("SELECT m.testname,r.patientid,r.dateofreq from CasualityLabResult n inner join LabSubTestMaster m on m.testmasterid=n.labmasterid inner join CasualityLabRequest r on r.labreqid=n.labreqid  inner join patientdetails p on p.patientid=r.patientid where n.labresid='" + ViewState["reqid"] + "'", c.Con);

            //   SqlCommand cmd = new SqlCommand("SELECT testname from NCDLabResult n inner join LabSubTestMaster m on m.testmasterid=n.labmasterid  where n.labresid='"+ViewState["reqid"]+"'", c.Con);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                lbl_test.Text = dt.Rows[0][0].ToString();
                lbl_date.Text = dt.Rows[0][2].ToString();
                lbl_pat.Text = dt.Rows[0][1].ToString();
            }
            c.Con.Close();
        }
    }
    protected void btn_sub_Click(object sender, EventArgs e)
    {
        c.getCon();
        SqlCommand cmdupd = new SqlCommand("update CasualityLabResult set result='" + txt_test.Text + "',dateofres='" + DateTime.Today + "',verifiedtechid='" + Session["uid"] + "' where labresid='" + ViewState["reqid"] + "'", c.Con);
        cmdupd.ExecuteNonQuery();
        SqlCommand cmd = new SqlCommand("SELECT m.testmasterid,m.testname,n.labresid from CasualityLabResult n inner join LabSubTestMaster m on m.testmasterid=n.labmasterid inner join CasualityLabRequest r on r.labreqid=n.labreqid  inner join patientdetails p on p.patientid=r.patientid where n.dateofres=' ' and r.patientid=@pno and r.dateofreq=@date and r.status=@status", c.Con);

        // SqlCommand cmd = new SqlCommand("SELECT * from NCDLabRequest n inner join LabTestMaster m on m.testmasterid=n.testmasterid inner join patientdetails p on p.patientid=n.patientid inner join NCDEmployeeDetails e on e.empid=n.docid where n.patientid=@pno and dateofreq=@date and n.status=@status", c.Con);
        cmd.Parameters.AddWithValue("@pno", lbl_pat.Text);
        cmd.Parameters.AddWithValue("@date", lbl_date.Text);
        cmd.Parameters.AddWithValue("@status", "Paid");

        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            Response.Redirect(String.Format("EnterResultPatCas.aspx?PatID={0}&Date={1}", lbl_pat.Text, lbl_date.Text));
        }
        else
            Response.Redirect(String.Format("EnterResults.aspx"));

        c.Con.Close();

    }
}
