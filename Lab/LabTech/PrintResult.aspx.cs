﻿//print result
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;
using System.Text;
public partial class Lab_LabTech_PrintResult : System.Web.UI.Page
{
    conclass c = new conclass();
    string uname = " ";
    protected void Page_Load(object sender, EventArgs e)
    {

       if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Lab/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
        
            lbl_cas.Visible = false;
            btn_print.Visible = false;
            lbl_pno.Visible = false;
            lbl_name.Visible = false;
            lbl_age.Visible = false;
            lbl_sex.Visible = false;
            lbl_req.Visible = false;
            lbl_date.Visible = false;
            lbl_hosp.Visible = false;
            lbl_place.Visible = false;
            lbl_post.Visible = false;
        }
    }


    protected void btn_search_Click(object sender, EventArgs e)
    {
        c.getCon();
        SqlCommand cmd = new SqlCommand("SELECT * from EmployeeDetails where empid='"+Session["uid"]+"'", c.Con);
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);
        if (dt.Rows.Count > 0)
        {
            uname = dt.Rows[0][2].ToString();
            c.Con.Close();
            if (ddl_dept.SelectedItem.Text == "NCD")
            {
                bind_grid_ncd();

            }
            else if (ddl_dept.SelectedItem.Text == "OP")
            {
                bind_grid_op();

            }
            else if (ddl_dept.SelectedItem.Text == "Casuality")
            {
                bind_grid_cas();
            }
        }
    }
    public void bind_grid_cas()
    {

        c.getCon();
        SqlCommand cmd = new SqlCommand("SELECT * from CasualityLabRequest c inner join CasualityLabResult n on n.labreqid=c.labreqid inner join LabSubTestMaster m on m.testmasterid=n.labmasterid inner join patientdetails p on p.patientid=c.patientid inner join CasualityEmployeeDetails e on e.empid=c.docid where (c.patientid=@pno and dateofreq=@date and c.status=@status) or (c.patientid=@pno and dateofreq=@date and c.status=@stat)", c.Con);
        cmd.Parameters.AddWithValue("@pno", txt_id.Text);
        cmd.Parameters.AddWithValue("@date", txtDate.Text);
        cmd.Parameters.AddWithValue("@status", "Completed");

        cmd.Parameters.AddWithValue("@stat", "Paid");



        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            lbl_pno.Visible = true;
            lbl_name.Visible = true;
            lbl_age.Visible = true;
            lbl_sex.Visible = true;
            lbl_req.Visible = true;
            lbl_date.Visible = true;
            btn_print.Visible = true;
            lbl_hosp.Visible = true;
            lbl_place.Visible = true;
            lbl_hosp.Text = "Government General Hospital,";
            lbl_place.Text = "Changanacherry";
            lbl_pno.Text += ":" + dt.Rows[0][3];
            lbl_name.Text += ":" + dt.Rows[0][19];
            DateTime dob = Convert.ToDateTime(dt.Rows[0][25]);
            int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;

            lbl_age.Text += ":" + age;
            lbl_sex.Text += ":" + dt.Rows[0][32];
            lbl_date.Text += ":" + dt.Rows[0][5];
            lbl_req.Text += ":" + dt.Rows[0][35];

            gridview_pat.DataSource = dt;
            gridview_pat.DataBind();
            lbl_post.Visible = true;
            lbl_post.Text = "Lab technician: "+uname.ToString();
        }
        else
            lbl_cas.Visible = true;
        c.Con.Close();



    }

    public void bind_grid_op()
    {

        c.getCon();
        SqlCommand cmd = new SqlCommand("SELECT * from LabRequest c inner join LabResult n on n.labreqid=c.labreqid inner join LabSubTestMaster m on m.testmasterid=n.labmasterid inner join patientdetails p on p.patientid=c.patientid inner join EmployeeDetails e on e.empid=c.docid where (c.patientid=@pno and dateofreq=@date and c.status=@status) or (c.patientid=@pno and dateofreq=@date and c.status=@stat)", c.Con);
        cmd.Parameters.AddWithValue("@pno", txt_id.Text);
        cmd.Parameters.AddWithValue("@date", txtDate.Text);
        cmd.Parameters.AddWithValue("@status", "Completed");
        cmd.Parameters.AddWithValue("@stat", "Paid");




        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            lbl_hosp.Text = "Government General Hospital,";
            lbl_place.Text = "Changanacherry";
            lbl_pno.Visible = true;
            lbl_name.Visible = true;
            lbl_age.Visible = true;
            lbl_sex.Visible = true;
            lbl_req.Visible = true;
            lbl_date.Visible = true;
            btn_print.Visible = true;
            lbl_hosp.Visible = true;
            lbl_place.Visible = true;
            lbl_pno.Text += ":" + dt.Rows[0][3];
            lbl_name.Text += ":" + dt.Rows[0][19];
            DateTime dob = Convert.ToDateTime(dt.Rows[0][25]);
            int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;

            lbl_age.Text += ":" + age;
            lbl_sex.Text += ":" + dt.Rows[0][32];
            DateTime dat = Convert.ToDateTime(dt.Rows[0][5]);
            string dats = dat.ToShortDateString();
            lbl_date.Text += ":" + dats;
            lbl_req.Text += ":" + dt.Rows[0][35];

            gridview_pat.DataSource = dt;
            gridview_pat.DataBind();
            lbl_post.Visible = true;
            lbl_post.Text = "Lab technician: " + uname.ToString();
        }
        else
            lbl_cas.Visible = true;


        c.Con.Close();

    }

    public void bind_grid_ncd()
    {

        c.getCon();
        SqlCommand cmd = new SqlCommand("SELECT * from  NCDLabRequest c inner join NCDLabResult n on n.labreqid=c.labreqid inner join LabSubTestMaster m on m.testmasterid=n.labmasterid inner join patientdetails p on p.patientid=c.patientid inner join NCDEmployeeDetails e on e.empid=c.docid where (c.patientid=@pno and dateofreq=@date and c.status=@status) or (c.patientid=@pno and dateofreq=@date and c.status=@stat)", c.Con);
        cmd.Parameters.AddWithValue("@pno", txt_id.Text);
        cmd.Parameters.AddWithValue("@date", txtDate.Text);
        cmd.Parameters.AddWithValue("@status", "Paid");
        cmd.Parameters.AddWithValue("@stat", "Completed");

        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            lbl_pno.Visible = true;
            lbl_name.Visible = true;
            lbl_age.Visible = true;
            lbl_sex.Visible = true;
            lbl_req.Visible = true;
            lbl_date.Visible = true;
            btn_print.Visible = true;
            lbl_hosp.Visible = true;
            lbl_hosp.Text = "Government General Hospital,";
            lbl_place.Text = "Changanacherry";
            lbl_place.Visible = true;
            lbl_pno.Text += ":" + dt.Rows[0][3];
            lbl_name.Text += ":" + dt.Rows[0][19];
            DateTime dob = Convert.ToDateTime(dt.Rows[0][25]);
            int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;

            lbl_age.Text += ":" + age;
            lbl_sex.Text += ":" + dt.Rows[0][32];
            lbl_date.Text += ":" + dt.Rows[0][5];
            lbl_req.Text += ":" + dt.Rows[0][35];

            gridview_pat.DataSource = dt;
            gridview_pat.DataBind();
            lbl_post.Visible = true;
            lbl_post.Text = "Lab technician: " + uname.ToString();
        }
        else
            lbl_cas.Visible = true;
        c.Con.Close();



    }
    public void btnExportPDF()
    {
        //c.getCon();

        Document document = new Document(PageSize.A4, 88f, 88f, 20f, 20f);
        Font NormalFont = FontFactory.GetFont("Arial", 14, Font.NORMAL, Color.BLACK);
        using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
        {
            PdfWriter writer = PdfWriter.GetInstance(document, memoryStream);
            Phrase phrase = null;
            PdfPCell cell = null;
            PdfPTable table = null;
            Color color = null;

            document.Open();

            //Header Table
            table = new PdfPTable(2);
            table.TotalWidth = 500f;
            table.LockedWidth = true;
            table.SetWidths(new float[] { 0.3f, 0.7f });

            //Company Logo
            cell = ImageCell("~/OPD/Registration/image/gov.png", 30f, PdfPCell.ALIGN_CENTER);
            table.AddCell(cell);

            //Company Name and Address
            phrase = new Phrase();
            phrase.Add(new Chunk("Government General Hospital,\n\n", FontFactory.GetFont("Arial", 16, Font.BOLD, Color.BLACK)));
            phrase.Add(new Chunk("Changanacherry,\n", FontFactory.GetFont("Arial", 12, Font.NORMAL, Color.BLACK)));
            phrase.Add(new Chunk("Kottayam,\n", FontFactory.GetFont("Arial", 10, Font.NORMAL, Color.BLACK)));
            phrase.Add(new Chunk("Kerala-686101", FontFactory.GetFont("Arial", 10, Font.NORMAL, Color.BLACK)));
            cell = PhraseCell(phrase, PdfPCell.ALIGN_LEFT);
            cell.VerticalAlignment = PdfCell.ALIGN_TOP;
            table.AddCell(cell);

            //Separater Line
            color = new Color(System.Drawing.ColorTranslator.FromHtml("#A9A9A9"));
            DrawLine(writer, 25f, document.Top - 79f, document.PageSize.Width - 25f, document.Top - 79f, color);
            DrawLine(writer, 25f, document.Top - 80f, document.PageSize.Width - 25f, document.Top - 80f, color);
            document.Add(table);

            table = new PdfPTable(2);
            table.HorizontalAlignment = Element.ALIGN_LEFT;
            table.SetWidths(new float[] { 0.3f, 1f });
            table.SpacingBefore = 20f;

            table = new PdfPTable(2);
            table.SetWidths(new float[] { 0.5f, 2f });
            table.TotalWidth = 340f;
            table.LockedWidth = true;
            table.SpacingBefore = 20f;
            table.HorizontalAlignment = Element.ALIGN_RIGHT;
            document.Add(table);

            SqlCommand cmd_post = new SqlCommand("select * from patientdetails where patientid= '" + txt_id.Text + "' ", c.Con);
            SqlDataAdapter sda_post = new SqlDataAdapter(cmd_post);
            DataTable dt_post = new DataTable();
            sda_post.Fill(dt_post);

            int k = cmd_post.ExecuteNonQuery();

            if (dt_post.Rows.Count > 0)
            {
                DateTime todaydate = DateTime.Now;
                string pname = Convert.ToString(dt_post.Rows[0][1]);
                DateTime dob = Convert.ToDateTime(dt_post.Rows[0][7]);
                int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;

                table.AddCell(PhraseCell(new Phrase("\n\n\nDate:", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                table.AddCell(PhraseCell(new Phrase("\n\n\n" + todaydate.ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
                cell.Colspan = 2;
                cell.PaddingBottom = 10f;
                table.AddCell(cell);

                table.AddCell(PhraseCell(new Phrase("Patient Id:", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                table.AddCell(PhraseCell(new Phrase(txt_id.Text, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
                cell.Colspan = 2;
                cell.PaddingBottom = 10f;
                table.AddCell(cell);

                table.AddCell(PhraseCell(new Phrase("Patient Name:", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                table.AddCell(PhraseCell(new Phrase(pname, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
                cell.Colspan = 2;
                cell.PaddingBottom = 10f;
                table.AddCell(cell);

                table.AddCell(PhraseCell(new Phrase("Age:", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                table.AddCell(PhraseCell(new Phrase(age.ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
                cell.Colspan = 2;
                cell.PaddingBottom = 10f;
                table.AddCell(cell);

                //table.AddCell(PhraseCell(new Phrase("Sex:", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                //table.AddCell(PhraseCell(new Phrase(lbl_sex.Text, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                //cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
                //cell.Colspan = 2;
                //cell.PaddingBottom = 10f;
                //table.AddCell(cell);

                //table.AddCell(PhraseCell(new Phrase("Requested By:", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                //table.AddCell(PhraseCell(new Phrase(lbl_req.Text, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                //cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
                //cell.Colspan = 2;
                //cell.PaddingBottom = 10f;
                //table.AddCell(cell);

                //table.AddCell(PhraseCell(new Phrase("Test Date:", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                //table.AddCell(PhraseCell(new Phrase(lbl_date.Text, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                //cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
                //cell.Colspan = 2;
                //cell.PaddingBottom = 10f;
                //table.AddCell(cell);
            }
            document.Add(table);

            PdfPTable table1 = new PdfPTable(3);
            table1.AddCell(PhraseCell(new Phrase("Description", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            table1.AddCell(PhraseCell(new Phrase("Result", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            table1.AddCell(PhraseCell(new Phrase("Normal Value", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            //table1.AddCell(PhraseCell(new Phrase("Frequency", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            //table1.AddCell(PhraseCell(new Phrase("Instructions", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
            cell.Colspan = 3;
            cell.PaddingBottom = 10f;
            table1.AddCell(cell);
            string pno = txt_id.Text;
            //SqlCommand cmd = new SqlCommand("select * from Prescription p inner join medMaster m on p.medid=m.MedId where patientid=@pno and dateofpres=@date and status=@stat order by presid", c.Con);
            //cmd.Parameters.AddWithValue("@pno", pno);
            //cmd.Parameters.AddWithValue("@date", DateTime.Today);
            //cmd.Parameters.AddWithValue("@stat", "Prescribed");
            //DataTable dt = new DataTable();
            //SqlDataAdapter da = new SqlDataAdapter(cmd);
            //da.Fill(dt);

            if (ddl_dept.SelectedItem.Text == "OP")
            {
                SqlCommand cmd = new SqlCommand("SELECT * from LabRequest c inner join LabResult n on n.labreqid=c.labreqid inner join LabSubTestMaster m on m.testmasterid=n.labmasterid where (c.patientid=@pno and dateofreq=@date and c.status=@status) or (c.patientid=@pno and dateofreq=@date and c.status=@stat)", c.Con);
                cmd.Parameters.AddWithValue("@pno", txt_id.Text);
                cmd.Parameters.AddWithValue("@date", txtDate.Text);
                cmd.Parameters.AddWithValue("@status", "Completed");
                cmd.Parameters.AddWithValue("@stat", "Paid");




                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);


                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][14].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                        table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][10].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                        table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][15].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                        cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
                        cell.Colspan = 3;
                        cell.PaddingBottom = 10f;
                        table1.AddCell(cell);
                    }
                }
                document.Add(table1);
            }
            else  if (ddl_dept.SelectedItem.Text == "OP")
            {
                SqlCommand cmd = new SqlCommand("SELECT * from CasualityLabRequest c inner join CasualityLabResult n on n.labreqid=c.labreqid inner join LabSubTestMaster m on m.testmasterid=n.labmasterid  where (c.patientid=@pno and dateofreq=@date and c.status=@status) or (c.patientid=@pno and dateofreq=@date and c.status=@stat)", c.Con);
                cmd.Parameters.AddWithValue("@pno", txt_id.Text);
                cmd.Parameters.AddWithValue("@date", txtDate.Text);
                cmd.Parameters.AddWithValue("@status", "Completed");
                cmd.Parameters.AddWithValue("@stat", "Paid");




                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);


                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][14].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                        table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][10].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                        table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][15].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                        cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
                        cell.Colspan = 3;
                        cell.PaddingBottom = 10f;
                        table1.AddCell(cell);
                    }
                }
                document.Add(table1);
            }
            else if (ddl_dept.SelectedItem.Text == "NCD")
            {
                SqlCommand cmd = new SqlCommand("SELECT * from NCDLabRequest c inner join NCDLabResult n on n.labreqid=c.labreqid inner join LabSubTestMaster m on m.testmasterid=n.labmasterid  where (c.patientid=@pno and dateofreq=@date and c.status=@status) or (c.patientid=@pno and dateofreq=@date and c.status=@stat)", c.Con);
                cmd.Parameters.AddWithValue("@pno", txt_id.Text);
                cmd.Parameters.AddWithValue("@date", txtDate.Text);
                cmd.Parameters.AddWithValue("@status", "Completed");
                cmd.Parameters.AddWithValue("@stat", "Paid");




                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);


                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][14].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                        table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][10].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                        table1.AddCell(PhraseCell(new Phrase(dt.Rows[i][15].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
                        cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
                        cell.Colspan = 3;
                        cell.PaddingBottom = 10f;
                        table1.AddCell(cell);
                    }
                }
                document.Add(table1);
            }
            //table.AddCell(PhraseCell(new Phrase("Lab Technician:", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            //table.AddCell(PhraseCell(new Phrase(lbl_post.Text, FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            //cell = PhraseCell(new Phrase(), PdfPCell.ALIGN_CENTER);
            //cell.Colspan = 2;
            //cell.PaddingBottom = 10f;
            //table.AddCell(cell);
            //document.Add(table1);

            ////Addtional Information
            //table.AddCell(PhraseCell(new Phrase("Addtional Information:", FontFactory.GetFont("Arial", 8, Font.BOLD, Color.BLACK)), PdfPCell.ALIGN_LEFT));
            //table.AddCell(PhraseCell(new Phrase(dr["Notes"].ToString(), FontFactory.GetFont("Arial", 8, Font.NORMAL, Color.BLACK)), PdfPCell.ALIGN_JUSTIFIED));
            //document.Add(table);
            document.Close();
            byte[] bytes = memoryStream.ToArray();
            memoryStream.Close();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("Content-Disposition", "attachment; filename=labresult.pdf");
            Response.ContentType = "application/pdf";
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(bytes);
            Response.End();
            Response.Close();
            //c.Con.Close();

        }
    }

    private static void DrawLine(PdfWriter writer, float x1, float y1, float x2, float y2, Color color)
    {
        PdfContentByte contentByte = writer.DirectContent;
        contentByte.SetColorStroke(color);
        contentByte.MoveTo(x1, y1);
        contentByte.LineTo(x2, y2);
        contentByte.Stroke();
    }
    private static PdfPCell PhraseCell(Phrase phrase, int align)
    {
        PdfPCell cell = new PdfPCell(phrase);
        cell.BorderColor = Color.WHITE;
        cell.VerticalAlignment = PdfCell.ALIGN_TOP;
        cell.HorizontalAlignment = align;
        cell.PaddingBottom = 2f;
        cell.PaddingTop = 0f;
        return cell;
    }
    private static PdfPCell ImageCell(string path, float scale, int align)
    {
        iTextSharp.text.Image image = iTextSharp.text.Image.GetInstance(HttpContext.Current.Server.MapPath(path));
        image.ScalePercent(scale);
        PdfPCell cell = new PdfPCell(image);
        cell.BorderColor = Color.WHITE;
        cell.VerticalAlignment = PdfCell.ALIGN_TOP;
        cell.HorizontalAlignment = align;
        cell.PaddingBottom = 0f;
        cell.PaddingTop = 0f;
        return cell;
    }
    protected void btn_print_Click(object sender, EventArgs e)
    {
        c.getCon();
        btnExportPDF();
        c.Con.Close();
    }
}