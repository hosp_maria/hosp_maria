﻿//view lab requests
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
public partial class Lab_LabTech_ViewRequest : System.Web.UI.Page
{
    conclass c = new conclass();
    protected void Page_Load(object sender, EventArgs e)
    {
         
       if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Lab/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
        
             lbl_cas.Visible = false;
             btn_print.Visible = false;
             lbl_pno.Visible = false;
             lbl_name.Visible = false;
             lbl_age.Visible = false;
             lbl_sex.Visible = false;
             lbl_req.Visible = false;
             lbl_date.Visible = false;
             lbl_hosp.Visible = false;
             lbl_place.Visible = false;
            btn_paid.Visible = false;
        }
    }

  
    protected void btn_search_Click(object sender, EventArgs e)
    {
        if(ddl_dept.SelectedItem.Text=="NCD")
        bind_grid_ncd();
        else if(ddl_dept.SelectedItem.Text=="OP")
            bind_grid_op();
        else if (ddl_dept.SelectedItem.Text == "Casuality")
            bind_grid_cas();
    }
    public void bind_grid_cas()
    {

        c.getCon();
        SqlCommand cmd = new SqlCommand("SELECT * from CasualityLabRequest n inner join LabTestMaster m on m.testmasterid=n.testmasterid inner join patientdetails p on p.patientid=n.patientid inner join CasualityEmployeeDetails e on e.empid=n.docid where n.patientid=@pno and dateofreq=@date and n.status=@status", c.Con);
        cmd.Parameters.AddWithValue("@pno", txt_id.Text);
        cmd.Parameters.AddWithValue("@date", txtDate.Text);
        cmd.Parameters.AddWithValue("@status", "Requested");




        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            lbl_pno.Visible = true;
            lbl_name.Visible = true;
            lbl_age.Visible = true;
            lbl_sex.Visible = true;
            lbl_req.Visible = true;
            lbl_date.Visible = true;
            btn_print.Visible = true;
            lbl_hosp.Visible = true;
            lbl_place.Visible = true;
            lbl_hosp.Text = "Government General Hospital,";
            lbl_place.Text="Changanacherry";
            btn_paid.Visible = true;
            lbl_pno.Text += ":" + dt.Rows[0][3];
            lbl_name.Text += ":" + dt.Rows[0][13];
            DateTime dob = Convert.ToDateTime(dt.Rows[0][19]);
            int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;

            lbl_age.Text += ":" + age;
            lbl_sex.Text += ":" + dt.Rows[0][26];
            lbl_date.Text += ":" + dt.Rows[0][5];
            lbl_req.Text += ":" + dt.Rows[0][29];

            gridview_pat.DataSource = dt;
            gridview_pat.DataBind();
            int total = dt.AsEnumerable().Sum(row => row.Field<int>("price"));
            gridview_pat.FooterRow.Cells[0].Text = "Total";
            gridview_pat.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            gridview_pat.FooterRow.Cells[1].Text = total.ToString("N2");
            lbl_tot.Text = "Total Price: Rs.";
            lbl_totval.Text = total + ".00";
        }
        else
            lbl_cas.Visible = true;




    }

    public void bind_grid_op()
    {

        c.getCon();
        SqlCommand cmd = new SqlCommand("SELECT * from LabRequest n inner join LabTestMaster m on m.testmasterid=n.testmasterid inner join patientdetails p on p.patientid=n.patientid inner join EmployeeDetails e on e.empid=n.docid where n.patientid=@pno and dateofreq=@date and n.status=@status", c.Con);
        cmd.Parameters.AddWithValue("@pno", txt_id.Text);
        cmd.Parameters.AddWithValue("@date", txtDate.Text);
        cmd.Parameters.AddWithValue("@status", "Requested");




        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            lbl_hosp.Text = "Government General Hospital,";
            lbl_place.Text = "Changanacherry";
            lbl_pno.Visible = true;
            lbl_name.Visible = true;
            lbl_age.Visible = true;
            lbl_sex.Visible = true;
            lbl_req.Visible = true;
            lbl_date.Visible = true;
            btn_print.Visible = true;
            lbl_hosp.Visible = true;
            lbl_place.Visible = true;
            btn_paid.Visible = true;
            lbl_pno.Text += ":" + dt.Rows[0][3];
            lbl_name.Text += ":" + dt.Rows[0][13];
            DateTime dob = Convert.ToDateTime(dt.Rows[0][19]);
            int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;

            lbl_age.Text += ":" + age;
            lbl_sex.Text += ":" + dt.Rows[0][26];
            lbl_date.Text += ":" + dt.Rows[0][5];
            lbl_req.Text += ":" + dt.Rows[0][29];

            gridview_pat.DataSource = dt;
            gridview_pat.DataBind();
            int total = dt.AsEnumerable().Sum(row => row.Field<int>("price"));
            gridview_pat.FooterRow.Cells[0].Text = "Total";
            gridview_pat.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            gridview_pat.FooterRow.Cells[1].Text = total.ToString("N2");
            lbl_tot.Text = "Total Price: Rs.";
            lbl_totval.Text = total + ".00";
        }
        else
            lbl_cas.Visible = true;




    }

    public void bind_grid_ncd()
    {

        c.getCon();
        SqlCommand cmd = new SqlCommand("SELECT * from NCDLabRequest n inner join LabTestMaster m on m.testmasterid=n.testmasterid inner join patientdetails p on p.patientid=n.patientid inner join NCDEmployeeDetails e on e.empid=n.docid where n.patientid=@pno and dateofreq=@date and n.status=@status", c.Con);
         cmd.Parameters.AddWithValue("@pno", txt_id.Text);
         cmd.Parameters.AddWithValue("@date",  txtDate.Text);
         cmd.Parameters.AddWithValue("@status", "Requested");


        

        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            lbl_pno.Visible = true;
            lbl_name.Visible = true;
            lbl_age.Visible = true;
            lbl_sex.Visible = true;
            lbl_req.Visible = true;
            lbl_date.Visible = true;
            btn_print.Visible = true;
            lbl_hosp.Visible = true;
            lbl_hosp.Text = "Government General Hospital,";
            lbl_place.Text = "Changanacherry";
            lbl_place.Visible = true;
            btn_paid.Visible = true;
            lbl_pno.Text += ":" + dt.Rows[0][3];
            lbl_name.Text += ":" + dt.Rows[0][13];
            DateTime dob = Convert.ToDateTime(dt.Rows[0][19]);
            int age = ((DateTime.Now.Year - dob.Year) * 372 + (DateTime.Now.Month - dob.Month) * 31 + (DateTime.Now.Day - dob.Day)) / 372;
           
            lbl_age.Text += ":" + age;
            lbl_sex.Text += ":" + dt.Rows[0][26];
            lbl_date.Text += ":" + dt.Rows[0][5];
            lbl_req.Text += ":" + dt.Rows[0][29];

            gridview_pat.DataSource = dt;
            gridview_pat.DataBind();
             int total = dt.AsEnumerable().Sum(row => row.Field<int>("price"));
                        gridview_pat.FooterRow.Cells[0].Text = "Total";
                        gridview_pat.FooterRow.Cells[0].HorizontalAlign = HorizontalAlign.Right;
                        gridview_pat.FooterRow.Cells[1].Text = total.ToString("N2");
                        lbl_tot.Text = "Total Price: Rs.";
                        lbl_totval.Text = total+".00";
        }
        else
            lbl_cas.Visible = true;
        
 

    
    }
    protected void btn_paid_Click(object sender, EventArgs e)
    {
        c.getCon();
        if (ddl_dept.SelectedItem.Text == "OP")
        {
            SqlCommand cmd = new SqlCommand("SELECT * from LabRequest n inner join LabTestMaster m on m.testmasterid=n.testmasterid  where n.patientid=@pno and dateofreq=@date and status=@status", c.Con);
            cmd.Parameters.AddWithValue("@pno", txt_id.Text);
            cmd.Parameters.AddWithValue("@date", txtDate.Text);
            cmd.Parameters.AddWithValue("@status", "Requested");


            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    SqlCommand cmds = new SqlCommand("update LabRequest set status='Paid' where labreqid='" + dt.Rows[i][0] + "'", c.Con);
                    cmds.ExecuteNonQuery();
                    SqlCommand cmd_res = new SqlCommand("SELECT * from LabSubTestMaster where testmaster='" + dt.Rows[i][1] + "'", c.Con);
                    DataTable dt_res = new DataTable();
                    SqlDataAdapter da_res = new SqlDataAdapter(cmd_res);
                    da_res.Fill(dt_res);


                    if (dt_res.Rows.Count > 0)
                    {
                        for (int j = 0; j < dt_res.Rows.Count; j++)
                        {
                            SqlCommand cmd_lab = new SqlCommand("insert into LabResult values('" + dt.Rows[i][0] + "', '" + dt_res.Rows[j][0] + "',' ',' ',' ')", c.Con);
                            cmd_lab.ExecuteNonQuery();

                        }
                    }
                }
            }
        }
        else if (ddl_dept.SelectedItem.Text == "NCD")
        {
            SqlCommand cmd = new SqlCommand("SELECT * from NCDLabRequest n inner join LabTestMaster m on m.testmasterid=n.testmasterid  where n.patientid=@pno and dateofreq=@date and status=@status", c.Con);
            cmd.Parameters.AddWithValue("@pno", txt_id.Text);
            cmd.Parameters.AddWithValue("@date", txtDate.Text);
            cmd.Parameters.AddWithValue("@status", "Requested");


            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    SqlCommand cmds = new SqlCommand("update NCDLabRequest set status='Paid' where labreqid='" + dt.Rows[i][0] + "'", c.Con);
                    cmds.ExecuteNonQuery();
                    SqlCommand cmd_res = new SqlCommand("SELECT * from LabSubTestMaster where testmaster='" + dt.Rows[i][1] + "'", c.Con);
                    DataTable dt_res = new DataTable();
                    SqlDataAdapter da_res = new SqlDataAdapter(cmd_res);
                    da_res.Fill(dt_res);


                    if (dt_res.Rows.Count > 0)
                    {
                        for (int j = 0; j < dt_res.Rows.Count; j++)
                        {
                            SqlCommand cmd_lab = new SqlCommand("insert into NCDLabResult values('" + dt.Rows[i][0] + "', '" + dt_res.Rows[j][0] + "',' ',' ',' ')", c.Con);
                            cmd_lab.ExecuteNonQuery();

                        }
                    }
                }
            }
        }
        else if (ddl_dept.SelectedItem.Text == "Casuality")
        {

            SqlCommand cmd = new SqlCommand("SELECT * from CasualityLabRequest n inner join LabTestMaster m on m.testmasterid=n.testmasterid  where n.patientid=@pno and dateofreq=@date and status=@status", c.Con);
            cmd.Parameters.AddWithValue("@pno", txt_id.Text);
            cmd.Parameters.AddWithValue("@date", txtDate.Text);
            cmd.Parameters.AddWithValue("@status", "Requested");


            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(dt);


            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    SqlCommand cmds = new SqlCommand("update CasualityLabRequest set status='Paid' where labreqid='" + dt.Rows[i][0] + "'", c.Con);
                    cmds.ExecuteNonQuery();
                    SqlCommand cmd_res = new SqlCommand("SELECT * from LabSubTestMaster where testmaster='" + dt.Rows[i][1] + "'", c.Con);
                    DataTable dt_res = new DataTable();
                    SqlDataAdapter da_res = new SqlDataAdapter(cmd_res);
                    da_res.Fill(dt_res);


                    if (dt_res.Rows.Count > 0)
                    {
                        for (int j = 0; j < dt_res.Rows.Count; j++)
                        {
                            SqlCommand cmd_lab = new SqlCommand("insert into CasualityLabResult values('" + dt.Rows[i][0] + "', '" + dt_res.Rows[j][0] + "',' ',' ',' ')", c.Con);
                            cmd_lab.ExecuteNonQuery();

                        }
                    }
                }
            }
        }

        c.Con.Close();
    }
}