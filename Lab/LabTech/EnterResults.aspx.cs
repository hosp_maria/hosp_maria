﻿//view all tests to be checked
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
public partial class Lab_LabTech_EnterResults : System.Web.UI.Page
{
    conclass c = new conclass();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["Id"] == null)
                Response.Redirect("~/Lab/loginpage.aspx");
            else
            {
                Response.ClearHeaders();
                Response.AddHeader("Cache-Control", "no-cache, no-store, max-age=0, must-revalidate");
                Response.AddHeader("Pragma", "no-cache");
            }
        
            lbl_cas.Visible = false;
           
        }
    }


    protected void btn_search_Click(object sender, EventArgs e)
    {
        if (ddl_dept.SelectedItem.Text == "NCD")
            bind_grid_ncd();
        else if (ddl_dept.SelectedItem.Text == "OP")
            bind_grid_op();
        else if (ddl_dept.SelectedItem.Text == "Casuality")
            bind_grid_cas();
    }
    public void bind_grid_cas()
    {

        c.getCon();
        SqlCommand cmd = new SqlCommand("SELECT m.testmasterid,m.testname,n.labresid from CasualityLabResult n inner join LabSubTestMaster m on m.testmasterid=n.labmasterid inner join CasualityLabRequest r on r.labreqid=n.labreqid  inner join patientdetails p on p.patientid=r.patientid where n.dateofres=' ' and r.patientid=@pno and r.dateofreq=@date and r.status=@status", c.Con);

       // SqlCommand cmd = new SqlCommand("SELECT * from CasualityLabRequest n inner join LabTestMaster m on m.testmasterid=n.testmasterid inner join patientdetails p on p.patientid=n.patientid inner join CasualityEmployeeDetails e on e.empid=n.docid where n.patientid=@pno and dateofreq=@date and n.status=@status", c.Con);
        cmd.Parameters.AddWithValue("@pno", txt_id.Text);
        cmd.Parameters.AddWithValue("@date", txtDate.Text);
        cmd.Parameters.AddWithValue("@status", "Paid");




        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
            gridview_pat.DataSource = dt;
            gridview_pat.DataBind();
            
        }
        else
            lbl_cas.Visible = true;

    }

    public void bind_grid_op()
    {

        c.getCon();
        SqlCommand cmd = new SqlCommand("SELECT m.testmasterid,m.testname,n.labresid from LabResult n inner join LabSubTestMaster m on m.testmasterid=n.labmasterid inner join LabRequest r on r.labreqid=n.labreqid  inner join patientdetails p on p.patientid=r.patientid where n.dateofres=' ' and r.patientid=@pno and r.dateofreq=@date and r.status=@status", c.Con);

       //SqlCommand cmd = new SqlCommand("SELECT * from LabRequest n inner join LabTestMaster m on m.testmasterid=n.testmasterid inner join patientdetails p on p.patientid=n.patientid inner join EmployeeDetails e on e.empid=n.docid where n.patientid=@pno and dateofreq=@date and n.status=@status", c.Con);
        cmd.Parameters.AddWithValue("@pno", txt_id.Text);
        cmd.Parameters.AddWithValue("@date", txtDate.Text);
        cmd.Parameters.AddWithValue("@status", "Paid");

        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
          
            gridview_pat.DataSource = dt;
            gridview_pat.DataBind();
           
        }
        else
            lbl_cas.Visible = true;

    }

    public void bind_grid_ncd()
    {

        c.getCon();
        SqlCommand cmd = new SqlCommand("SELECT m.testmasterid,m.testname,n.labresid from NCDLabResult n inner join LabSubTestMaster m on m.testmasterid=n.labmasterid inner join NCDLabRequest r on r.labreqid=n.labreqid  inner join patientdetails p on p.patientid=r.patientid where n.dateofres=' ' and r.patientid=@pno and r.dateofreq=@date and r.status=@status", c.Con);

       // SqlCommand cmd = new SqlCommand("SELECT * from NCDLabRequest n inner join LabTestMaster m on m.testmasterid=n.testmasterid inner join patientdetails p on p.patientid=n.patientid inner join NCDEmployeeDetails e on e.empid=n.docid where n.patientid=@pno and dateofreq=@date and n.status=@status", c.Con);
        cmd.Parameters.AddWithValue("@pno", txt_id.Text);
        cmd.Parameters.AddWithValue("@date", txtDate.Text);
        cmd.Parameters.AddWithValue("@status", "Paid");

        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        da.Fill(dt);


        if (dt.Rows.Count > 0)
        {
           
            gridview_pat.DataSource = dt;
            gridview_pat.DataBind();
           
        }
        else
            lbl_cas.Visible = true;

    }
   
    protected void gridview_pat_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (ddl_dept.SelectedItem.Text == "OP")
        {
            if (e.CommandName == "Select")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                Response.Redirect("ResEnter.aspx?ReqId=" + gridview_pat.DataKeys[index].Value.ToString());

            }
        
        }   else
                 if (ddl_dept.SelectedItem.Text == "Casuality")
        {
            if (e.CommandName == "Select")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                Response.Redirect("ResEnterCas.aspx?ReqId=" + gridview_pat.DataKeys[index].Value.ToString());

            }
        }
                 else
                 if (ddl_dept.SelectedItem.Text == "NCD")
        {
            if (e.CommandName == "Select")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                Response.Redirect("ResEnterNcd.aspx?ReqId=" + gridview_pat.DataKeys[index].Value.ToString());

            }
        }
    }
}